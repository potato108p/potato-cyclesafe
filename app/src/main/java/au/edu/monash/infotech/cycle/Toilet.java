package au.edu.monash.infotech.cycle;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Toilet bean, store toilet info
 * Created by james on 12/4/17.
 */

public class Toilet {
    private String id;
    private String name;
    private String addressNote;
    private String isMale;
    private String isFemale;
    private String inUnisex;
    private String facilityType;
    private String accessNote;
    private String isOpen;
    private String openningHoursSchedule;
    private String openningHours_Note;
    private String babyChange;
    private String shower;
    private String drinkingWater;
    private String sharpsDisposal;
    private String sanitaryDisposal;
    private LatLng latLng;

    public Toilet() {
    }

    public Toilet(String id, String name, String addressNote, String isMale, String isFemale, String inUnisex, String facilityType, String accessNote, String isOpen, String openningHoursSchedule, String openningHours_Note, String babyChange, String shower, String drinkingWater, String sharpsDisposal, String sanitaryDisposal, LatLng latLng) {
        this.id = id;
        this.name = name;
        this.addressNote = addressNote;
        this.isMale = isMale;
        this.isFemale = isFemale;
        this.inUnisex = inUnisex;
        this.facilityType = facilityType;
        this.accessNote = accessNote;
        this.isOpen = isOpen;
        this.openningHoursSchedule = openningHoursSchedule;
        this.openningHours_Note = openningHours_Note;
        this.babyChange = babyChange;
        this.shower = shower;
        this.drinkingWater = drinkingWater;
        this.sharpsDisposal = sharpsDisposal;
        this.sanitaryDisposal = sanitaryDisposal;
        this.latLng = latLng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }

    public String getIsMale() {
        return isMale;
    }

    public void setIsMale(String isMale) {
        this.isMale = isMale;
    }

    public String getIsFemale() {
        return isFemale;
    }

    public void setIsFemale(String isFemale) {
        this.isFemale = isFemale;
    }

    public String getInUnisex() {
        return inUnisex;
    }

    public void setInUnisex(String inUnisex) {
        this.inUnisex = inUnisex;
    }

    public String getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

    public String getAccessNote() {
        return accessNote;
    }

    public void setAccessNote(String accessNote) {
        this.accessNote = accessNote;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    public String getOpenningHoursSchedule() {
        return openningHoursSchedule;
    }

    public void setOpenningHoursSchedule(String openningHoursSchedule) {
        this.openningHoursSchedule = openningHoursSchedule;
    }

    public String getOpenningHours_Note() {
        return openningHours_Note;
    }

    public void setOpenningHours_Note(String openningHours_Note) {
        this.openningHours_Note = openningHours_Note;
    }

    public String getBabyChange() {
        return babyChange;
    }

    public void setBabyChange(String babyChange) {
        this.babyChange = babyChange;
    }

    public String getShower() {
        return shower;
    }

    public void setShower(String shower) {
        this.shower = shower;
    }

    public String getDrinkingWater() {
        return drinkingWater;
    }

    public void setDrinkingWater(String drinkingWater) {
        this.drinkingWater = drinkingWater;
    }

    public String getSharpsDisposal() {
        return sharpsDisposal;
    }

    public void setSharpsDisposal(String sharpsDisposal) {
        this.sharpsDisposal = sharpsDisposal;
    }

    public String getSanitaryDisposal() {
        return sanitaryDisposal;
    }

    public void setSanitaryDisposal(String sanitaryDisposal) {
        this.sanitaryDisposal = sanitaryDisposal;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
