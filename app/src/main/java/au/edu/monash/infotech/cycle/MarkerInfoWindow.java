package au.edu.monash.infotech.cycle;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

/**
 * Customized info window, using IconGenerator
 * Created by james on 21/4/17.
 */

public class MarkerInfoWindow {

    private Context context;

    private static final Drawable TRANSPARENT_DRAWABLE = new ColorDrawable(Color.TRANSPARENT);


    public MarkerInfoWindow(Context context) {
        this.context = context;
    }

    public Marker putMarkerWithTheftInfo(GoogleMap googleMap,String temp, LatLng latLng) {
        IconGenerator iconGenerator = new IconGenerator(context);
        iconGenerator.setContentView(getWhiteViewWithPlus(temp));
        //iconGenerator.setBackground(TRANSPARENT_DRAWABLE);
        return markPlaceOnMap(googleMap, latLng, iconGenerator);
    }

    private Marker markPlaceOnMap(GoogleMap googleMap, LatLng latLng, IconGenerator iconGenerator) {
        MarkerOptions markerOptions = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon())) //BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()
                .position(latLng)
                .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV());

        return googleMap.addMarker(markerOptions);
    }

    private View getWhiteViewWithPlus(String temp) {
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_layout, null);
        TextView tvInfoTemp = (TextView) view.findViewById(R.id.tvInfoTemp);
        //String t = String.valueOf((int) (Double.parseDouble(temp) - 273.15));
        tvInfoTemp.setText(String.valueOf((int) (Double.parseDouble(temp) - 273.15)) + "\u2103");
        return view;
    }

}
