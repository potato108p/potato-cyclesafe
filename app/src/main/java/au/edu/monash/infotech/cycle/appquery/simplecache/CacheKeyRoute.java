package au.edu.monash.infotech.cycle.appquery.simplecache;

import com.google.android.gms.maps.model.LatLng;

import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Created by Moody on 5/05/2017.
 */

public class CacheKeyRoute extends CacheKey {
    final LatLng end;

    public CacheKeyRoute(int radius, Unit unit, double startLatitude, double startLongitude, double endLatitude, double endLongitude, String servletName) {
        super(radius, unit, startLatitude, startLongitude, servletName);
        this.end = new LatLng(endLatitude, endLongitude);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CacheKeyRoute c = (CacheKeyRoute) o;

        return ((this.radius == c.radius) && (this.unit == c.unit) &&
                (this.start.latitude == c.start.latitude) &&
                (this.start.longitude == c.start.longitude)) &&
                (this.end.latitude == c.end.latitude) &&
                (this.end.longitude == c.end.longitude) &&
                (this.servletName.equals(c.servletName));
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(radius, unit, start.latitude, start.longitude, end.latitude, end.longitude, servletName);
    }
}
