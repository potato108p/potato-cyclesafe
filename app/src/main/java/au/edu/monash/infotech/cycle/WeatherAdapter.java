package au.edu.monash.infotech.cycle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.List;

import static au.edu.monash.infotech.cycle.R.id.imIcon;

/**
 * Weather adapter for weather fragment
 * Created by james on 15/4/17.
 */

public class WeatherAdapter extends BaseAdapter {
    private List<WeatherForecast> weatherForecastList;
    private LayoutInflater layoutInflater;

    public WeatherAdapter(Context context, List<WeatherForecast> data) {
        weatherForecastList = data;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return weatherForecastList.size();
    }

    @Override
    public Object getItem(int position) {
        return weatherForecastList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_layout, null);
            viewHolder.tvTime = (TextView) convertView.findViewById(R.id.itemTime);
            viewHolder.tvTemp = (TextView) convertView.findViewById(R.id.itemTemp);
            viewHolder.tvWind = (TextView) convertView.findViewById(R.id.itemWind);
            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.itemDescription);
            viewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.itemIcon);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Ion.with(viewHolder.ivIcon).load("http://openweathermap.org/img/w/" + weatherForecastList.get(position).getIcon() + ".png");
        viewHolder.tvTime.setText(weatherForecastList.get(position).getTime());
        viewHolder.tvTemp.setText(weatherForecastList.get(position).getTemp() + "\u2103");
        viewHolder.tvWind.setText("Wind Speed: " + weatherForecastList.get(position).getWindSpeed() + " m/s - Direction: " + weatherForecastList.get(position).getWindDir());
        viewHolder.tvDescription.setText(weatherForecastList.get(position).getDescription());
        return convertView;
    }

    class ViewHolder{
        public TextView tvTime, tvTemp, tvWind, tvDescription;
        public ImageView ivIcon;
    }
}
