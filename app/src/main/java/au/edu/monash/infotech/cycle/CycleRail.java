package au.edu.monash.infotech.cycle;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Rail bean, store cycle rail info
 * Created by james on 17/4/17.
 */

public class CycleRail {
    private List<String> descriptions;
    private List<LatLng> latLngs;
    private String status;

    public CycleRail() {
    }

    public CycleRail(List<String> descriptions, List<LatLng> latLngs, String status) {
        this.descriptions = descriptions;
        this.latLngs = latLngs;
        this.status = status;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<String> descriptions) {
        this.descriptions = descriptions;
    }

    public List<LatLng> getLatLngs() {
        return latLngs;
    }

    public void setLatLngs(List<LatLng> latLngs) {
        this.latLngs = latLngs;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
