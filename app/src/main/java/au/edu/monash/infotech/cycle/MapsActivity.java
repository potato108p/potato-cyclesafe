package au.edu.monash.infotech.cycle;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
//import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import au.edu.monash.infotech.cycle.appquery.CycleSafeQueriesHOPP;
import au.edu.monash.infotech.cycle.common.SchoolInfo;
import au.edu.monash.infotech.cycle.shared.DayOfWeek;
import au.edu.monash.infotech.cycle.shared.Hour;
import au.edu.monash.infotech.cycle.shared.Month;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.Unit;
import me.drakeet.materialdialog.MaterialDialog;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.support.v7.appcompat.R.id.never;
import static android.support.v7.appcompat.R.id.time;
import static android.view.inputmethod.EditorInfo.IME_ACTION_DONE;
import static android.view.inputmethod.EditorInfo.IME_ACTION_NEXT;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMyLocationButtonClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;
    public static final int RADIUS_CURRENT_LOCATION = 500;
    public static final int RADIUS_ROUTE = 200;
    public static final int SCHOOL_RADIUS_ROUTE = 400;
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?mode=bicycling&region=au&avoid=highways";
    private static final String PLACE_URL_API = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=";
    private static final String WEATHER_URL_API = "http://api.openweathermap.org/data/2.5/weather?lat=";
    private static final String WEATHER_FORECAST_URL_API = "http://api.openweathermap.org/data/2.5/forecast?lat=";
    private static final String GOOGLE_API_KEY = "AIzaSyCJsVsu5DIYWXUlVn0YJYlRxHHNuCrVfyg";
    private static final String WEATHER_API_KEY = "247b61e9636e6a34f82544c3222ff6b5";
    private GoogleMap mMap;
    protected GoogleApiClient client;
    private LocationManager mLocationManager;

    //private EditText etOrigin;
    //private Button btStart, btReset;
    private FancyButton fbStart, fbReset;
    //private ProgressBar progressBar;
    private FloatingActionButton fabSchool, fabHotSpot, fabToilet, fabFountain, fabParking;
    private FloatingActionsMenu fabMenu;
    private AVLoadingIndicatorView avi;

    private AutoCompleteTextView avOrigin, avDestination;
    private PlaceAutocompleteAdapter mAdapter;

    private List<LatLng> poly = new ArrayList<>();
    private List<Marker> schoolMarkers = new ArrayList<>();
    private List<Marker> accidentMarkers = new ArrayList<>();
    private List<Marker> toiletMarker = new ArrayList<>();
    private List<Marker> fountainMarker = new ArrayList<>();
    private List<Marker> parkingMarker = new ArrayList<>();

    private Marker markerOrigin, markerDestination, touchDestination, markerCurrentLoc;

    private LatLng llOrigin, llDestination, currentLoc;
    private Location updateLocation;

    //private ClusterManager<Marker> clusterManager;
    private HeatmapTileProvider mProvider;
    private TileOverlay mOverlay;

    //private final Calendar mCalendar = Calendar.getInstance();
    private WeatherFragment weatherFragment = new WeatherFragment();

    private String destStatus = null;

    private Boolean firstLoading = true;
    private Boolean schoolDisplay = false;
    private Boolean accidentDisplay = false;
    private Boolean toiletDisplay = false;
    private Boolean fountainDisplay = false;
    private Boolean parkingDisplay = false;

    private MarkerInfoWindow markerInfoWindow;

    private SharedPreferences prefs = null;

    private MaterialDialog materialDialog;

    private Fragment routeFragment;
    private HashMap<String, Toilet> hashMap = new HashMap<>();

    private static final LatLngBounds BOUNDS_GREATER_MELBOURNE = new LatLngBounds(new LatLng(-38.439576, 144.594297), new LatLng(-37.515142, 145.514402));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        prefs = getSharedPreferences("first_loading", 0);
        if (prefs.getBoolean("my_first_time", true)) {
            //the app is being launched for first time
            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);
            // record the fact that the app has been started at least once
            prefs.edit().putBoolean("my_first_time", false).commit();
        }

//        Intent intent = new Intent(this, IntroActivity.class);
//        startActivity(intent);

        // Hide route fragment
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        routeFragment = getFragmentManager().findFragmentById(R.id.routeFragment);
        ft.hide(routeFragment);
        ft.commit();

        //etOrigin = (ClearableEditText) findViewById(R.id.etOrigin);
        fbStart = (FancyButton) findViewById(R.id.fbStart);
        fbReset = (FancyButton) findViewById(R.id.fbReset);
        //progressBar = (ProgressBar) findViewById(R.id.pb);
        fabSchool = (FloatingActionButton) findViewById(R.id.fabSchool);
        fabHotSpot = (FloatingActionButton) findViewById(R.id.fabHotSpot);
        fabToilet = (FloatingActionButton) findViewById(R.id.fabToilet);
        fabFountain = (FloatingActionButton) findViewById(R.id.fabFountain);
        fabParking = (FloatingActionButton) findViewById(R.id.fabParking);
        fabMenu = (FloatingActionsMenu) findViewById(R.id.fabMenu);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);

        markerInfoWindow = new MarkerInfoWindow(this);

        buildGoogleApiClient();

        avOrigin = (AutoCompleteTextView) findViewById(R.id.avOrigin);
        avDestination = (ClearableAutoCompleteTextView) findViewById(R.id.avDestination);
        avDestination.setSelection(avDestination.getText().length());
        // Register a listener that receives callbacks when a suggestion has been selected
        avOrigin.setOnItemClickListener(mAutocompleteClickListener);
        avDestination.setOnItemClickListener(mAutocompleteClickListener);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API
        mAdapter = new PlaceAutocompleteAdapter(this, client, BOUNDS_GREATER_MELBOURNE, null);
        avOrigin.setAdapter(mAdapter);
        avDestination.setAdapter(mAdapter);

        // Press ENTER, focus moves to etDestination
        avOrigin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER)) {
                    avDestination.requestFocus();
                    return true;
                }
                return false;
            }
        });

        // Press ENTER, focus move to btStart and execute
        avDestination.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) { //actionId == EditorInfo.IME_ACTION_DONE
                    fbStart.requestFocus();
                    startDirection();
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(avDestination.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });


        fbStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide the keyboard after clicking start button
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(fbStart.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                startDirection();
            }
        });

        fbReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Clear the map
                cleanMap();
                mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.map_style));
                // Hide route fragment
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                routeFragment = getFragmentManager().findFragmentById(R.id.routeFragment);
                ft.hide(routeFragment);
                ft.commit();
                //avi.smoothToHide();
                poly = new ArrayList<>();
                // Update current location info, clear destination text and move camera
                if (updateLocation != null) {
                    currentLoc = new LatLng(updateLocation.getLatitude(), updateLocation.getLongitude());
                    goToLocation(currentLoc);
                    avDestination.setText("");
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        List<Address> addressList = geocoder.getFromLocation(updateLocation.getLatitude(), updateLocation.getLongitude(), 1);
                        avOrigin.setText(addressList.get(0).getAddressLine(0) + ", "
                                + addressList.get(0).getLocality() + ", "
                                + addressList.get(0).getAdminArea() + ", "
                                + addressList.get(0).getCountryName());
                        String postCode = addressList.get(0).getPostalCode();
                        resetCurrentLocInfoWindow(postCode);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    avDestination.requestFocus();
                } else {
                    Toast.makeText(getApplicationContext(), "Error: can not access current location!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Expend the fab menu after loading
        fabMenu.toggle();

        fabSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Display school markers, check the status of school fab
                if (!schoolDisplay) {        //no school markers
                    //Check, if the route is planned, search schools around the route, else, search schools around current location
                    if (!poly.isEmpty()) {
                        //new SchoolAsyncTask().execute();
                        new SchoolInfoAsyncTask().execute();
                    } else if (currentLoc != null) {
//                        String lat = String.valueOf(currentLoc.latitude);
//                        String lng = String.valueOf(currentLoc.longitude);
//                        String placeAPI = PLACE_URL_API + lat + "," + lng + "&radius=" + RADIUS_CURRENT_LOCATION + "&types=school&key=" + GOOGLE_API_KEY;
//                        new PlaceAsyncTask().execute(placeAPI);
                        new SchoolInfoAsyncTask().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Loading location, please wait!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.map_style));
                    //clear markers and reverse fab status
                    deleteMarkers(schoolMarkers);
                    schoolDisplay = false;
                    fabSchool.setColorNormalResId(R.color.white);
                    fabSchool.setColorPressedResId(R.color.white_pressed);
                }
            }
        });

        fabHotSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Display accident markers, check the status of accident fab
                if (!accidentDisplay) {
                    //Check, if the route is planned, search accidents around the route, else, search accidents around current location
                    if (!poly.isEmpty()) {
                        new AccidentAsyncTask().execute(); //After get the accidents near route, rewrite it
                    } else if (currentLoc != null) {
                        new AccidentAsyncTask().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Loading location, please wait!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //clear markers and reverse fab status
                    //deleteMarkers(accidentMarkers);
                    mOverlay.remove();
                    accidentDisplay = false;
                    fabHotSpot.setColorNormalResId(R.color.white);
                    fabHotSpot.setColorPressedResId(R.color.white_pressed);
                }
            }
        });

        fabToilet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Display toilet markers, check the status of toilet fab
                if (!toiletDisplay) {
                    //Check, if the route is planned, search toilets around the route, else, search toilets around current location
                    if (!poly.isEmpty()) {
                        new ToiletAsyncTask().execute();
                    } else if (currentLoc != null) {
                        new ToiletAsyncTask().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Loading location, please wait!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //clear markers and reverse fab status
                    deleteMarkers(toiletMarker);
                    toiletDisplay = false;
                    fabToilet.setColorNormalResId(R.color.white);
                    fabToilet.setColorPressedResId(R.color.white_pressed);
                }
            }
        });

        fabFountain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Display fountain markers, check the status of fountain fab
                if (!fountainDisplay) {
                    //Check, if the route is planned, search fountains around the route, else, search fountains around current location
                    if (!poly.isEmpty()) {
                        new FountainAsyncTask().execute();
                    } else if (currentLoc != null) {
                        new FountainAsyncTask().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Loading location, please wait!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //clear markers and reverse fab status
                    deleteMarkers(fountainMarker);
                    fountainDisplay = false;
                    fabFountain.setColorNormalResId(R.color.white);
                    fabFountain.setColorPressedResId(R.color.white_pressed);
                }
            }
        });

        fabParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Display parking markers, check the status of parking fab
                if (!parkingDisplay) {
                    //Check, if the route is planned, search parking around the route, else, search parking around current location
                    if (!poly.isEmpty()) {
                        new ParkingAsyncTask().execute();
                    } else if (currentLoc != null) {
                        new ParkingAsyncTask().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Loading location, please wait!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //clear markers and reverse fab status
                    deleteMarkers(parkingMarker);
                    parkingDisplay = false;
                    fabParking.setColorNormalResId(R.color.white);
                    fabParking.setColorPressedResId(R.color.white_pressed);
                }
            }
        });
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i("Auto", "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(client, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i("Auto", "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e("Auto", "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);
            place.getAddress();
            Log.i("Auto", (String) place.getAddress());

            places.release();
        }
    };

    /**
     * Set up the LocationManager and get the last known location
     * If the permission is denied, return the method
     */
    private void setupLocationManager() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //checkLocationPermission();
            return;
        }
        //location permissions: ACCESS_COARSE_LOCATION -- use WiFi or mobile cell data
        if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.i("Current", "One");
                    updateLocation = location;
                    currentLocation(location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
            //location permissions: ACCESS_FINE_LOCATION -- available location providers, including the Global Positioning System (GPS) as well as WiFi and mobile cell data
        } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.i("Current", "Two");
                    updateLocation = location;
                    currentLocation(location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
    }

    /**
     * Access current location info, use Geocoder to parse address and set textview
     *
     * @param location current location
     */
    private void currentLocation(Location location) {

        double lat = location.getLatitude();
        double lng = location.getLongitude();
        currentLoc = new LatLng(lat, lng);

        Geocoder geocoder = new Geocoder(getApplicationContext());
        try {
            if (firstLoading) {
                //Get the address info for current location
                List<Address> addressList = geocoder.getFromLocation(lat, lng, 1);
                String addressLine = addressList.get(0).getAddressLine(0);
                String postCode = addressList.get(0).getPostalCode();
                avOrigin.setText(addressLine + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName());
                //avOrigin.clearFocus();
                avDestination.requestFocus();
                //move the camera to current location
                goToLocation(currentLoc);
                resetCurrentLocInfoWindow(postCode);
                firstLoading = false; //do this method once only
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Display accident hot spot and update weather info base on post code
     * @param postCode post code of location
     */
    private void resetCurrentLocInfoWindow(String postCode) {
        //Display nearby accident base on current location when the app first loaded
        new AccidentAsyncTask().execute();
        //Access weather info and send it to weather fragment
        String weatherForecastUrl = WEATHER_FORECAST_URL_API + currentLoc.latitude + "&lon=" + currentLoc.longitude + "&appid=" + WEATHER_API_KEY;
        Weather weather = requestWeather(currentLoc);
        Bundle bundle = new Bundle();
        bundle.putParcelable("Object", weather);
        bundle.putString("Forecast", weatherForecastUrl);
        try {
            List<String> theft = new TheftAsyncTask().execute(postCode).get();
            if (theft.get(0).equals("0")) {
                bundle.putString("Theft", theft.get(1));
                Log.i("Theft", theft.get(1));
            } else {
                bundle.putString("Theft", "No theft data for this suburb");
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        weatherFragment.setArguments(bundle);
        if (weather != null) {
            markerCurrentLoc = markerInfoWindow.putMarkerWithTheftInfo(mMap, weather.getTemp(), currentLoc);
        } else {
            Toast.makeText(getApplicationContext(), "Weather Info Error!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * Use user input of origin and destination and google map direction api, pass to ParseAsyncTask
     */
    private void startDirection() {
        //clean the format of user input
        String origin = avOrigin.getText().toString().trim().replaceAll(" ", "");
        String destination = avDestination.getText().toString().trim().replaceAll(" ", "");
        //String addressReserve = null;

        //judge whether the start address is empty
        if (origin.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter origin address!", Toast.LENGTH_LONG).show();
            return;
        }

        //judge whether the destination address is empty, if not, use the coordinate chose by user first then use the entered address
        if (destination.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter destination address!", Toast.LENGTH_LONG).show();
            return;
        } else if (llDestination != null) {
            Geocoder geocoder = new Geocoder(getApplicationContext());
            try {
                List<Address> addressList = geocoder.getFromLocation(llDestination.latitude, llDestination.longitude, 1);
                String addressLine = addressList.get(0).getAddressLine(0);
                //if user touch screen to get destination before and change textview, use the value of textview to set the destination
                if (destStatus != null && !avDestination.getText().toString().equals(destStatus)) {
                    destination = avDestination.getText().toString().trim().replaceAll(" ", "");
//                    LatLng latLng = convertAddress(avDestination.getText().toString().trim().replaceAll(" ", ""));
//                    destination = latLng.latitude + "," + latLng.longitude;
//                    Log.i("Destination", "Cor: " + latLng.toString());
                    Log.i("Destination", "One");
                } else if (destStatus == null) {    //else if user never touch screen to get destination, use the value of textview to set the destination
                    destination = avDestination.getText().toString().trim().replaceAll(" ", "");
//                    LatLng latLng = convertAddress(avDestination.getText().toString().trim().replaceAll(" ", ""));
//                    destination = latLng.latitude + "," + latLng.longitude;
//                    Log.i("Destination", "Cor: " + latLng.toString());
                    Log.i("Destination", "Two");
                } else {     //else, use the coordinate access by touching the screen to set destination
                    avDestination.setText(addressLine + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName());
                    destination = llDestination.latitude + "," + llDestination.longitude;
                    Log.i("Destination", "Three");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.i("Destination", "destStatus:" + destStatus + "; etDestination:" + avDestination.getText().toString() + ";destination: " + destination);

        String jsonAPI = DIRECTION_URL_API + "&origin=" + origin + "&destination=" + destination + "&key=" + GOOGLE_API_KEY;
        new ParseAsyncTask().execute(jsonAPI);
    }

    /**
     * Create the polyline using the return data from google direction api
     *
     * @param lines list of Latlng of location
     */
    private void createPolyline(List<LatLng> lines) {
        //initialize fab and clear the map
        cleanMap();
        //create the polyline
        PolylineOptions polylineOptions = new PolylineOptions();
        for (int i = 0; i < lines.size(); i++) {
            polylineOptions.add(lines.get(i));
        }
        //set Latlng value of origin and destination
        llOrigin = lines.get(0);
        llDestination = lines.get(lines.size() - 1);
        //Move camera according to origin and destination
        LatLngBounds latLngBounds = LatLngBounds.builder().include(llOrigin).include(llDestination).build();
        goToLocationZoom(latLngBounds);
        //Remove current location marker once the route is planned
        if (markerCurrentLoc != null) {
            markerCurrentLoc.remove();
        }
        //get the post code of location then access the theft data
//        Geocoder geocoder = new Geocoder(getApplicationContext());
//        try {
//            List<Address> addressOrigin = geocoder.getFromLocation(llOrigin.latitude, llOrigin.longitude, 1);
//            List<Address> addressDestination = geocoder.getFromLocation(llDestination.latitude, llDestination.longitude, 1);
//            String postCodeOrigin = addressOrigin.get(0).getPostalCode();
//            try {
//                List<String> theft = new TheftAsyncTask().execute(postCodeOrigin).get();
//                Weather weatherOrigin = requestWeather(llOrigin);
//                if (theft.get(0).equals("0")) {
//                    markerOrigin = markerInfoWindow.putMarkerWithTheftInfo(mMap, weatherOrigin.getTemp(), llOrigin);
//                } else if (weatherOrigin != null) {
//                    markerOrigin = markerInfoWindow.putMarkerWithTheftInfo(mMap, weatherOrigin.getTemp(), llOrigin);
//                } else {
//                    Toast.makeText(getApplicationContext(), "No theft data found for origin!", Toast.LENGTH_LONG).show();
//                    markerOrigin = markerInfoWindow.putMarkerWithTheftInfo(mMap, "Temp", llOrigin);
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//            String postCodeDestination = addressDestination.get(0).getPostalCode();
//            try {
//                List<String> theft = new TheftAsyncTask().execute(postCodeDestination).get();
//                Weather weatherDestination = requestWeather(llDestination);
//                if (theft.get(0).equals("0") && weatherDestination != null) {
//                    markerDestination = markerInfoWindow.putMarkerWithTheftInfo(mMap, weatherDestination.getTemp(), llDestination);
//                } else {
//                    Toast.makeText(getApplicationContext(), "No theft data found for destination!", Toast.LENGTH_LONG).show();
//                    markerDestination = markerInfoWindow.putMarkerWithTheftInfo(mMap, weatherDestination.getTemp(), llDestination);
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Weather weatherOrigin = requestWeather(llOrigin);
        if (weatherOrigin != null) {
            markerOrigin = markerInfoWindow.putMarkerWithTheftInfo(mMap, weatherOrigin.getTemp(), llOrigin);
        } else {
            Toast.makeText(getApplicationContext(), "Error: origin weather", Toast.LENGTH_LONG).show();
        }
        Weather weatherDestination = requestWeather(llDestination);
        if (weatherDestination != null) {
            markerDestination = markerInfoWindow.putMarkerWithTheftInfo(mMap, weatherDestination.getTemp(), llDestination);
        } else {
            Toast.makeText(getApplicationContext(), "Error: destination weather", Toast.LENGTH_LONG).show();
        }
        mMap.addPolyline(polylineOptions.width(10).color(Color.argb(255, 65, 105, 225)));
    }

    /**
     * Move the camera to the location
     *
     * @param latLng the coordinate of location
     */
    private void goToLocation(LatLng latLng) {
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.moveCamera(update);
    }

    /**
     * Move the camera to the area
     *
     * @param latLngBounds location bounds with two coordinate
     */
    private void goToLocationZoom(LatLngBounds latLngBounds) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100));
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Set initial map display at central of Melbourne
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-37.8121, 144.9620), 13));

        //Check the version and get location permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //buildGoogleApiClient();
                setupLocationManager();
                mMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
                //mMap.setMyLocationEnabled(true);
            }
        } else {
            //buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            setupLocationManager();
        }
        //mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMarkerClickListener(this);

        //Get the location info by touching the map
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                LatLng point = latLng;
                avDestination.requestFocus();
                if (touchDestination == null) {
                    touchDestination = mMap.addMarker(new MarkerOptions().position(point).title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_current)));
                    llDestination = point;
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        List<Address> addressList = geocoder.getFromLocation(llDestination.latitude, llDestination.longitude, 1);
                        String addressLine = addressList.get(0).getAddressLine(0);
                        destStatus = addressLine + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName();
                        avDestination.setText(addressLine + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName());
                        //avDestination.setText("");
                        avDestination.clearFocus();
                        //btStart.requestFocus();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    touchDestination.remove();
                    touchDestination = mMap.addMarker(new MarkerOptions().position(point).title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_current)));
                    llDestination = point;
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        List<Address> addressList = geocoder.getFromLocation(llDestination.latitude, llDestination.longitude, 1);
                        String addressLine = addressList.get(0).getAddressLine(0);
                        destStatus = addressLine + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName();
                        avDestination.setText(addressLine + ", " + addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName());
                        //avDestination.setText("");
                        avDestination.clearFocus();
                        //btStart.requestFocus();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                //touchDestination.remove();
                //touchDestination = mMap.addMarker(new MarkerOptions().position(point).title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination)));
            }
        });

        //Set the map style
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
    }

    /**
     * Convert the address info to coordinate
     *
     * @param textToSearch address text
     * @return coordinate of address
     */
    private LatLng convertAddress(String textToSearch) {
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> fromLocationName = null;
        LatLng latLng = null;
        try {
            fromLocationName = geocoder.getFromLocationName(textToSearch, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (fromLocationName != null && fromLocationName.size() > 0) {
            Address a = fromLocationName.get(0);
            latLng = new LatLng(a.getLatitude(), a.getLongitude());
        }
        return latLng;
    }

    /**
     * Build Google client
     */
    public void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
        client.connect();
    }

    /**
     * Check whether the location permission is granted and show info dialog
     */
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the location permission, please accept")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                        setupLocationManager();
                        //mLocationManager.requestLocationUpdates();
                    }
                } else {
                    // permission denied
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * Delete list of marker
     *
     * @param markerList list of marker
     */
    private void deleteMarkers(List<Marker> markerList) {
        for (int i = 0; i < markerList.size(); i++) {
            markerList.get(i).remove();
        }
    }

    /**
     * Clean map, reset fabs, initialize polyline list and hide route fragment
     */
    private void cleanMap() {
        mMap.clear();
        //poly = new ArrayList<>();
        schoolDisplay = false;
        accidentDisplay = false;
        toiletDisplay = false;
        fountainDisplay = false;
        parkingDisplay = false;
        fabSchool.setColorNormalResId(R.color.white);
        fabSchool.setColorPressedResId(R.color.white_pressed);
        fabHotSpot.setColorNormalResId(R.color.white);
        fabHotSpot.setColorPressedResId(R.color.white_pressed);
        fabToilet.setColorNormalResId(R.color.white);
        fabToilet.setColorPressedResId(R.color.white_pressed);
        fabFountain.setColorNormalResId(R.color.white);
        fabFountain.setColorPressedResId(R.color.white_pressed);
        fabParking.setColorNormalResId(R.color.white);
        fabParking.setColorPressedResId(R.color.white_pressed);
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
//        routeFragment = getFragmentManager().findFragmentById(R.id.routeFragment);
//        ft.hide(routeFragment);
//        ft.commit();
    }

    private Weather requestWeather(LatLng latLng) {
        Weather weather = new Weather();
        String weatherApi = WEATHER_URL_API + latLng.latitude + "&lon=" + latLng.longitude + "&appid=" + WEATHER_API_KEY;
        String weatherForecastUrl = WEATHER_FORECAST_URL_API + latLng.latitude + "&lon=" + latLng.longitude + "&appid=" + WEATHER_API_KEY;
        try {
            weather = new WeatherAsyncTask().execute(weatherApi).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return weather;
    }

    /**
     * onClick listener for markers, use the location info to request weather api
     * sent the weather info to weather fragment and display the weather
     * @param marker origin or destination markers
     * @return click result
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        // Click listener for origin marker
        if (marker.equals(markerOrigin)) {
            Bundle bundle = new Bundle();
            Geocoder geocoder = new Geocoder(getApplicationContext());
            try {
                List<Address> addressOrigin = geocoder.getFromLocation(llOrigin.latitude, llOrigin.longitude, 1);
                String postCodeOrigin = addressOrigin.get(0).getPostalCode();
                try {
                    List<String> theft = new TheftAsyncTask().execute(postCodeOrigin).get();
                    if (theft.get(0).equals("0")) {
                        bundle.putString("Theft", theft.get(1));
                    } else {
                        bundle.putString("Theft", "No theft data for this suburb");
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            WeatherFragment weatherFragment = new WeatherFragment();
            weatherFragment.show(getSupportFragmentManager(), weatherFragment.getTag());
            String weatherApi = WEATHER_URL_API + llOrigin.latitude + "&lon=" + llOrigin.longitude + "&appid=" + WEATHER_API_KEY;
            String weatherForecastUrl = WEATHER_FORECAST_URL_API + llOrigin.latitude + "&lon=" + llOrigin.longitude + "&appid=" + WEATHER_API_KEY;
            try {
                Weather weather = new WeatherAsyncTask().execute(weatherApi).get();
                bundle.putParcelable("Object", weather);
                bundle.putString("Forecast", weatherForecastUrl);
                weatherFragment.setArguments(bundle);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        // Click listener for destination marker
        if (marker.equals(markerDestination)) {
            Bundle bundle = new Bundle();
            Geocoder geocoder = new Geocoder(getApplicationContext());
            try {
                List<Address> addressDestination = geocoder.getFromLocation(llDestination.latitude, llDestination.longitude, 1);
                String postCodeDestination = addressDestination.get(0).getPostalCode();
                try {
                    List<String> theft = new TheftAsyncTask().execute(postCodeDestination).get();
                    if (theft.get(0).equals("0")) {
                        bundle.putString("Theft", theft.get(1));
                    } else {
                        bundle.putString("Theft", "No theft data for this suburb");
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            WeatherFragment weatherFragment = new WeatherFragment();
            weatherFragment.show(getSupportFragmentManager(), weatherFragment.getTag());
            String weatherApi = WEATHER_URL_API + llDestination.latitude + "&lon=" + llDestination.longitude + "&appid=" + WEATHER_API_KEY;
            String weatherForecastUrl = WEATHER_FORECAST_URL_API + llOrigin.latitude + "&lon=" + llOrigin.longitude + "&appid=" + WEATHER_API_KEY;
            try {
                Weather weather = new WeatherAsyncTask().execute(weatherApi).get();
                Log.i("WWWWW", "City: " + weather.getCity());
                bundle.putParcelable("Object", weather);
                bundle.putString("Forecast", weatherForecastUrl);
                weatherFragment.setArguments(bundle);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        // Click listener for current location marker
        if (marker.equals(markerCurrentLoc)) {
            weatherFragment.show(getSupportFragmentManager(), weatherFragment.getTag());
        }
        // Click listener for toilet marker
        if (hashMap.get(marker.getId()) != null) {
            Toilet toilet = hashMap.get(marker.getId());
            materialDialog = new MaterialDialog(this)
                    .setTitle(toilet.getName())
                    .setMessage(formatToiletInfo(toilet))
                    .setCanceledOnTouchOutside(true)
                    .setPositiveButton("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialDialog.dismiss();
                        }
                    });
            materialDialog.show();
        }
        return false;
    }

    /**
     * Format dialog info
     * @param t Toilet object
     * @return formated text
     */
    private String formatToiletInfo(Toilet t) {
        String s = "Male: " + t.getIsMale()
                + "  |  Female: " + t.getIsFemale()
                + "  |  Unisex: " + t.getInUnisex();
        if (!t.getIsOpen().isEmpty()) {
            s = s + "\nAvailability: " + t.getIsOpen();
        }
        if (!t.getOpenningHoursSchedule().isEmpty()) {
            s = s + "\nSchedule: " + t.getOpenningHoursSchedule();
        }
        if (!t.getOpenningHours_Note().isEmpty()) {
            s = s + "\nNote: " + t.getOpenningHours_Note();
        }
        if (!t.getAddressNote().isEmpty()) {
            s = s + "\n" + t.getAddressNote();
        }
        if (!t.getAccessNote().isEmpty()) {
            s = s + "\n" + t.getAccessNote();
        }
        if (t.getBabyChange().equals("Yes")
                || t.getShower().equals("Yes")
                || t.getDrinkingWater().equals("Yes")
                || t.getSharpsDisposal().equals("Yes")
                || t.getSanitaryDisposal().equals("Yes")) {
            s = s + "\nFacility: ";
            if (t.getBabyChange().equals("Yes")) {
                s = s + "<Baby Change>";
            }
            if (t.getShower().equals("Yes")) {
                s = s + "<Shower>";
            }
            if (t.getDrinkingWater().equals("Yes")) {
                s = s + "<Drinking Water>";
            }
            if (t.getSharpsDisposal().equals("Yes")) {
                s = s + "<Sharp Disposal>";
            }
            if (t.getSanitaryDisposal().equals("Yes")) {
                s = s + "<Sanitary Disposal>";
            }
        }
        return s;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Auto", "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    /**
     * AsyncTask for planning a route using Google direction api
     */
    class ParseAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpHandler sh = new HttpHandler();
            return sh.makeServiceCall(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ParseJson pj = new ParseJson();
            DirectionInfo result = pj.ParseDirectionJson(s);
            if (result == null) {
                Toast.makeText(getApplicationContext(), "Location not found!", Toast.LENGTH_LONG).show();
                avi.smoothToHide();
                return;
            }
            DecodePolyline dp = new DecodePolyline();
            poly = dp.decodePoints(result.getPolyline());
            createPolyline(poly);
            TextView tvDistance = (TextView) findViewById(R.id.tvDistance);
            tvDistance.setText(result.getDistance());
            TextView tvDuration = (TextView) findViewById(R.id.tvDuration);
            tvDuration.setText(result.getDuration());

            if (routeFragment.isHidden()) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                routeFragment = getFragmentManager().findFragmentById(R.id.routeFragment);
                ft.show(routeFragment);
                ft.commit();
                avi.smoothToHide();
            }


        }
    }

    /**
     * AsyncTask for showing all the schools near the route using Google place api
     */
    class SchoolAsyncTask extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected List<String> doInBackground(Void... params) {
            List<String> result = new ArrayList<>();
            HttpHandler sh = new HttpHandler();
            for (int i = 0; i < poly.size(); i = i + 2) {
                String lat = String.valueOf(poly.get(i).latitude);
                String lng = String.valueOf(poly.get(i).longitude);
                String placeAPI = PLACE_URL_API + lat + "," + lng + "&radius=" + SCHOOL_RADIUS_ROUTE + "&types=school&key=" + GOOGLE_API_KEY;
                String jsonStr = sh.makeServiceCall(placeAPI);
                result.add(jsonStr);
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            super.onPostExecute(strings);
            List<School> schoolList = new ArrayList<>();
            for (int i = 0; i < strings.size(); i++) {
                ParseJson pj = new ParseJson();
                List<School> schools = pj.ParsePlaceJson(strings.get(i));
                schoolList.addAll(schools);
            }
//            HashSet h = new HashSet(schoolList);
//            schoolList.clear();
//            schoolList.addAll(h);
            for (int i = 0; i < schoolList.size(); i++) {
                School school = schoolList.get(i);
                double lat = school.getLat();
                double lng = school.getLng();
                String name = school.getName();
                Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school)));
                schoolMarkers.add(marker);
            }
            avi.smoothToHide();
            if (schoolList.size() == 0) {
                Toast.makeText(getApplicationContext(), "No schools found nearby!", Toast.LENGTH_LONG).show();
            } else {
                schoolDisplay = true;
                //tvSchoolState.setText("School On");
                fabSchool.setColorNormalResId(R.color.purple);
                fabSchool.setColorPressedResId(R.color.purple_pressed);
            }
        }
    }

    /**
     * AsyncTask for showing all the schools near the current location using Google place api
     */
    class PlaceAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpHandler sh = new HttpHandler();
            return sh.makeServiceCall(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ParseJson pj = new ParseJson();
            List<School> schools = pj.ParsePlaceJson(s);
            for (int i = 0; i < schools.size(); i++) {
                School school = schools.get(i);
                double lat = school.getLat();
                double lng = school.getLng();
                String name = school.getName();
                Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school)));
                schoolMarkers.add(marker);
            }
            avi.smoothToHide();
            if (schools.size() == 0) {
                Toast.makeText(getApplicationContext(), "No schools found nearby!", Toast.LENGTH_LONG).show();
            } else {
                schoolDisplay = true;
                //tvSchoolState.setText("School On");
                fabSchool.setColorNormalResId(R.color.purple);
                fabSchool.setColorPressedResId(R.color.purple_pressed);
            }
        }
    }

    /**
     * AsyncTask for showing all primary schools and high schools around current or the route
     */
    class SchoolInfoAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            avi.smoothToShow();
        }

        @Override
        protected String doInBackground(String... params) {
            CycleSafeQueriesHOPP cycleSafeQueriesHOPP = new CycleSafeQueriesHOPP();
            String s = "";
            if (!poly.isEmpty()) {
                List<Map<String, Double>> coordinateList = CycleSafeQueriesHOPP.convertLatLngToMap(poly);
                try {
                    s = cycleSafeQueriesHOPP.findSchoolsAroundRoute(SCHOOL_RADIUS_ROUTE, Unit.M, coordinateList).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (currentLoc != null) {
                double lat = currentLoc.latitude;
                double lng = currentLoc.longitude;
                try {
                    s = cycleSafeQueriesHOPP.findSchoolsAroundMidPoint(lat, lng, RADIUS_CURRENT_LOCATION, Unit.M).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return s;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ParseJson parseJson = new ParseJson();
            if (s.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_SHORT).show();
            } else {
                SchoolInfo schoolInfo = parseJson.ParseSchool(s);
                String status = schoolInfo.getStatus();
                if (status.equals("0")) {
                    List<LatLng> latLngList = schoolInfo.getLatLngList();
                    List<String> nameList = schoolInfo.getNameList();
                    for (int i = 0; i < latLngList.size(); i++) {
                        Marker marker = mMap.addMarker(new MarkerOptions().position(latLngList.get(i)).title(nameList.get(i)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school)));
                        schoolMarkers.add(marker);
                    }
                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.map_style_school));
                    schoolDisplay = true;
                    fabSchool.setColorNormalResId(R.color.purple);
                    fabSchool.setColorPressedResId(R.color.purple_pressed);
                } else if (status.equals("6")) {
                    Toast.makeText(getApplicationContext(), "No secondary or primary schools were found nearby!", Toast.LENGTH_LONG).show();
                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.map_style));
                } else {
                    errorMsg(status);
                }
            }
            //progressBar.setVisibility(View.GONE);
            avi.smoothToHide();
        }
    }

    /**
     * AsyncTask for showing all the accidents around current location or the route
     */
    class AccidentAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected String doInBackground(Void... params) {
            CycleSafeQueriesHOPP cycleSafeQueriesHOPP = new CycleSafeQueriesHOPP();
            String s = "";
            if (!poly.isEmpty()) {
                List<Map<String, Double>> coordinateList = CycleSafeQueriesHOPP.convertLatLngToMap(poly);
                try {
                    //Access time info
                    Calendar mCalendar = Calendar.getInstance();
                    int mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
                    //s = cycleSafeQueriesHOPP.findAccidentNodesAroundRoute(RADIUS_ROUTE, Unit.M, coordinateList).toString();
                    s = cycleSafeQueriesHOPP.findAccidentNodesAroundRoute(RADIUS_ROUTE, Unit.M, coordinateList, Hour.getHour(mHour)).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (currentLoc != null) {
                double lat = currentLoc.latitude;
                double lng = currentLoc.longitude;
                //Access accident data around the current location, current time
                try {
                    //Access time info
                    Calendar mCalendar = Calendar.getInstance();
                    int mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
                    s = cycleSafeQueriesHOPP.findAccidentNodesAroundMidPoint(lat, lng, RADIUS_CURRENT_LOCATION, Unit.M, Hour.getHour(mHour)).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return s;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ParseJson parseJson = new ParseJson();
            if (s.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Error (Empty)!", Toast.LENGTH_SHORT).show();
            } else {
                SafeInfo safeInfo = parseJson.ParseAccident(s);
                String status = safeInfo.getStatusCode();
                if (status.equals("0")) {
                    List<WeightedLatLng> latLngList = safeInfo.getLatLngs();
                    mProvider = new HeatmapTileProvider.Builder().weightedData(latLngList).radius(50).build();
                    //mProvider.setRadius(100);
                    mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
//                    for (int i = 0; i < latLngList.size(); i++) {
//                        LatLng latLng = latLngList.get(i);
//                        double lat = latLng.latitude;
//                        double lng = latLng.longitude;
//                        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("Hot Spot").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_warning)));
//                        accidentMarkers.add(marker);
//                    }
                    fabHotSpot.setColorNormalResId(R.color.red);
                    fabHotSpot.setColorPressedResId(R.color.red_pressed);
                    accidentDisplay = true;
                } else if (status.equals("6")) {
                    Toast.makeText(getApplicationContext(), "No nearby accidents were found!", Toast.LENGTH_LONG).show();
                } else {
                    errorMsg(status);
                }
            }
            avi.smoothToHide();
        }
    }

    /**
     * Dispaly error message when requesting server
     * @param status status code
     */
    private void errorMsg(String status) {
        switch (status) {
            case "1":
                Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_LONG).show();
                break;
            case "2":
                Toast.makeText(getApplicationContext(), "SQL Error!", Toast.LENGTH_LONG).show();
                break;
            case "3":
                Toast.makeText(getApplicationContext(), "Missing Parameter!", Toast.LENGTH_LONG).show();
                break;
            case "4":
                Toast.makeText(getApplicationContext(), "Invalid Parameter!", Toast.LENGTH_LONG).show();
                break;
            case "5":
                Toast.makeText(getApplicationContext(), "JSON Error!", Toast.LENGTH_LONG).show();
                break;
//            case "6":
//                Toast.makeText(getApplicationContext(), "Result not found!", Toast.LENGTH_SHORT).show();
//                break;
            default:
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * AsyncTask for showing all the toilets around current location or the route
     */
    class ToiletAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected String doInBackground(Void... params) {
            CycleSafeQueriesHOPP cycleSafeQueriesHOPP = new CycleSafeQueriesHOPP();
            String s = "";
            if (!poly.isEmpty()) {
                List<Map<String, Double>> coordinateList = CycleSafeQueriesHOPP.convertLatLngToMap(poly);
                try {
                    s = cycleSafeQueriesHOPP.findToiletsAroundRoute(RADIUS_ROUTE, Unit.M, coordinateList).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (currentLoc != null) {
                double lat = currentLoc.latitude;
                double lng = currentLoc.longitude;
                try {
                    s = cycleSafeQueriesHOPP.findToiletsAroundMidPoint(lat, lng, RADIUS_CURRENT_LOCATION, Unit.M).toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return s;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ParseJson parseJson = new ParseJson();
            if (s.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_SHORT).show();
            } else {
                //mMap.setInfoWindowAdapter(new ToiletInfoWindowAdapter());
                ToiletHolder toiletHolder = parseJson.ParseToilet(s);
                String status = toiletHolder.getStatus();
                if (status.equals("0")) {
                    List<Toilet> toiletList = toiletHolder.getToiletList();
                    for (int i = 0; i < toiletList.size(); i++) {
                        Toilet toilet = toiletList.get(i);
                        Marker marker = mMap.addMarker(new MarkerOptions().position(toilet.getLatLng()).title(toilet.getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_toilet_cyan)));
                        toiletMarker.add(marker);
                        hashMap.put(marker.getId(), toilet);
                    }
                    fabToilet.setColorNormalResId(R.color.cyan);
                    fabToilet.setColorPressedResId(R.color.cyan_pressed);
                    toiletDisplay = true;
                } else if (status.equals("6")) {
                    Toast.makeText(getApplicationContext(), "No nearby toilets were found!", Toast.LENGTH_LONG).show();
                } else {
                    errorMsg(status);
                }
            }
            avi.smoothToHide();
        }
    }

    private class ToiletInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        private View view;

        public ToiletInfoWindowAdapter() {
            view = getLayoutInflater().inflate(R.layout.toilet_infowindow_layout, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            Toilet toilet = hashMap.get(marker.getId());
            TextView iwName = (TextView) view.findViewById(R.id.iwName);
            iwName.setText(toilet.getName());
            return view;
        }
    }

    /**
     * AsyncTask for showing all the fountains around current location or the route
     */
    class FountainAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected String doInBackground(Void... params) {
            CycleSafeQueriesHOPP cycleSafeQueriesHOPP = new CycleSafeQueriesHOPP();
            String s = "";
            if (!poly.isEmpty()) {
                List<Map<String, Double>> coordinateList = CycleSafeQueriesHOPP.convertLatLngToMap(poly);
                try {
                    s = cycleSafeQueriesHOPP.findDrinkingFountainsAroundRoute(RADIUS_ROUTE, Unit.M, coordinateList).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (currentLoc != null) {
                double lat = currentLoc.latitude;
                double lng = currentLoc.longitude;
                try {
                    s = cycleSafeQueriesHOPP.findDrinkingFountainsAroundMidPoint(lat, lng, RADIUS_CURRENT_LOCATION, Unit.M).toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return s;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ParseJson parseJson = new ParseJson();
            if (s.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_SHORT).show();
            } else {
//                mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//                    @Override
//                    public View getInfoWindow(Marker marker) {
//                        return null;
//                    }
//
//                    @Override
//                    public View getInfoContents(Marker marker) {
//                        return null;
//                    }
//                });
                Fountain fountain = parseJson.ParseFountain(s);
                String status = fountain.getStatus();
                if (status.equals("0")) {
                    List<LatLng> latLngList = fountain.getLatLngList();
                    for (int i = 0; i < latLngList.size(); i++) {
                        LatLng latLng = latLngList.get(i);
                        double lat = latLng.latitude;
                        double lng = latLng.longitude;
                        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("Drinking Fountain").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_drink)));
                        fountainMarker.add(marker);
                    }
                    fabFountain.setColorNormalResId(R.color.lightBlue);
                    fabFountain.setColorPressedResId(R.color.lightBlue_pressed);
                    fountainDisplay = true;
                } else if (status.equals("6")) {
                    Toast.makeText(getApplicationContext(), "No nearby fountains were found!", Toast.LENGTH_LONG).show();
                } else {
                    errorMsg(status);
                }
            }
            avi.smoothToHide();
        }
    }

    /**
     * AsyncTask for showing all the building with parking and cycle rail around current location
     */
    class ParkingAsyncTask extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected List<String> doInBackground(Void... params) {
            CycleSafeQueriesHOPP cycleSafeQueriesHOPP = new CycleSafeQueriesHOPP();
            List<String> s = new ArrayList<>();
            if (!poly.isEmpty()) {
                List<Map<String, Double>> coordinateList = CycleSafeQueriesHOPP.convertLatLngToMap(poly);
                try {
                    String building = cycleSafeQueriesHOPP.findBuildingsWithBicycleParkingAroundRoute(RADIUS_ROUTE, Unit.M, coordinateList).toString();
                    String rail = cycleSafeQueriesHOPP.findCycleRailsAroundRoute(RADIUS_ROUTE, Unit.M, coordinateList).toString();
                    s.add(building);
                    s.add(rail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (currentLoc != null) {
                double lat = currentLoc.latitude;
                double lng = currentLoc.longitude;
                try {
                    String building = cycleSafeQueriesHOPP.findBuildingsWithBicycleParkingAroundMidPoint(lat, lng, RADIUS_CURRENT_LOCATION, Unit.M).toString();
                    String rail = cycleSafeQueriesHOPP.findCycleRailsAroundMidPoint(lat, lng, RADIUS_CURRENT_LOCATION, Unit.M).toString();
                    s.add(building);
                    s.add(rail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return s;
        }

        @Override
        protected void onPostExecute(List<String> s) {
            super.onPostExecute(s);
            ParseJson parseJson = new ParseJson();
            if (s.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_SHORT).show();
            } else {
                String building = s.get(0);
                String rail = s.get(1);
                BuildingBicycle buildingBicycle = parseJson.ParseBuildingWithParking(building);
                CycleRail cycleRail = parseJson.PareCycleRail(rail);
                String statusBuilding = buildingBicycle.getStatus();
                String statusRail = cycleRail.getStatus();
                if (statusBuilding.equals("0") || statusRail.equals("0")) {
                    List<LatLng> latLngListBuilding = buildingBicycle.getLatLngList();
                    List<String> descriptionListBuilding = buildingBicycle.getDescription();
                    List<String> spaceNumListBuilding = buildingBicycle.getSpaceNum();
                    for (int i = 0; i < latLngListBuilding.size(); i++) {
                        LatLng latLng = latLngListBuilding.get(i);
                        String description = descriptionListBuilding.get(i);
                        String spaceNum = spaceNumListBuilding.get(i);
                        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(description).snippet("Number of Spaces: " + spaceNum).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_building_blue_grey)));
                        parkingMarker.add(marker);
                    }
                    List<LatLng> latLngListRail = cycleRail.getLatLngs();
                    List<String> descriptionListRail = cycleRail.getDescriptions();
                    for (int i = 0; i < latLngListRail.size(); i++) {
                        LatLng latLng = latLngListRail.get(i);
                        String description = descriptionListRail.get(i);
                        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title("Rail").snippet(description).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_parking_blue_grey)));
                        parkingMarker.add(marker);
                    }
                    fabParking.setColorNormalResId(R.color.blue_grey);
                    fabParking.setColorPressedResId(R.color.blue_grey_pressed);
                    parkingDisplay = true;
                } else if (statusBuilding.equals("6") && statusRail.equals("6")) {
                    Toast.makeText(getApplicationContext(), "No nearby parking were found!", Toast.LENGTH_LONG).show();
                    //errorMsg(status);
                }
            }
            avi.smoothToHide();
        }
    }

    /**
     * AsyncTask for showing the theft info
     */
    class TheftAsyncTask extends AsyncTask<String, Void, List<String>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected List<String> doInBackground(String... params) {
            CycleSafeQueriesHOPP cycleSafeQueriesHOPP = new CycleSafeQueriesHOPP();
            ParseJson pj = new ParseJson();
            List<String> theft = new ArrayList<>();
            try {
                String s = cycleSafeQueriesHOPP.getSafetyRatingForSuburb(Integer.parseInt(params[0])).toString();
                theft = pj.ParseTheft(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return theft;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            super.onPostExecute(strings);
            avi.smoothToHide();
        }
    }

    /**
     * AsyncTask for accessing weather info using OpenWeatherMap api
     */
    class WeatherAsyncTask extends AsyncTask<String, Void, Weather> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.smoothToShow();
        }

        @Override
        protected Weather doInBackground(String... params) {
            HttpHandler sh = new HttpHandler();
            String jsonStr = sh.makeServiceCall(params[0]);
            ParseJson pj = new ParseJson();
            return pj.ParseWeather(jsonStr);
        }

        @Override
        protected void onPostExecute(Weather w) {
            super.onPostExecute(w);
            Log.i("WWWWW", "T: " + w.getTemp());
//            ParseJson pj = new ParseJson();
//            String weatherInfo = pj.ParseWeather(s);
            //Log.i("Weather", "Temp: " + weatherInfo);
            //tvWeather.setText("Temp: " + weatherInfo);
            avi.smoothToHide();
        }
    }

}
