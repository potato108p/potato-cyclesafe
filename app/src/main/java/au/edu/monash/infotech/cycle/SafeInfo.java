package au.edu.monash.infotech.cycle;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Accident bean, store accident info
 * Created by james on 7/4/17.
 */

public class SafeInfo {
    List<WeightedLatLng> latLngs;
    String statusCode;

    public SafeInfo() {
    }

    public SafeInfo(List<WeightedLatLng> latLngs, String statusCode) {
        this.latLngs = latLngs;
        this.statusCode = statusCode;
    }

    public List<WeightedLatLng> getLatLngs() {
        return latLngs;
    }

    public void setLatLngs(List<WeightedLatLng> latLngs) {
        this.latLngs = latLngs;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
