package au.edu.monash.infotech.cycle;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Parcelable of weather info
 * Created by james on 8/4/17.
 */

public class Weather implements Parcelable {
    private String description;
    private String temp;
    private String pressure;
    private String humidity;
    private String windSpeed;
    private String windDirection;
    private String cloud;
    private String visibility;
    private String time;
    private String url;
    private String city;


    public Weather() {

    }

    public Weather(String description, String temp, String pressure, String humidity, String windSpeed, String windDirection, String cloud, String visibility, String time, String url, String city) {
        this.description = description;
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.cloud = cloud;
        this.visibility = visibility;
        this.time = time;
        this.url = url;
        this.city = city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
        dest.writeString(temp);
        dest.writeString(pressure);
        dest.writeString(humidity);
        dest.writeString(windSpeed);
        dest.writeString(windDirection);
        dest.writeString(cloud);
        dest.writeString(visibility);
        dest.writeString(time);
        dest.writeString(url);
        dest.writeString(city);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public String getCloud() {
        return cloud;
    }

    public void setCloud(String cloud) {
        this.cloud = cloud;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public static final Parcelable.Creator<Weather> CREATOR = new Creator() {

        @Override
        public Weather createFromParcel(Parcel source) {
            Weather weather = new Weather();
            weather.setDescription(source.readString());
            weather.setTemp(source.readString());
            weather.setPressure(source.readString());
            weather.setHumidity(source.readString());
            weather.setWindSpeed(source.readString());
            weather.setWindDirection(source.readString());
            weather.setCloud(source.readString());
            weather.setVisibility(source.readString());
            weather.setTime(source.readString());
            weather.setUrl(source.readString());
            weather.setCity(source.readString());
            return weather;
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };
}
