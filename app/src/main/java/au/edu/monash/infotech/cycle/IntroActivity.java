package au.edu.monash.infotech.cycle;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * App Intro class, display tutorial for first-time loading
 * Created by james on 29/4/17.
 */

public class IntroActivity extends AppIntro {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(AppIntroFragment.newInstance("Get Started!", "Plan your daily cycling routes using the map feature.\n Enter your destination or long press on the map to start planning your journey.", R.drawable.slide_1, getResources().getColor(R.color.colorPrimaryDark)));
        addSlide(AppIntroFragment.newInstance("Menu Icon", "Show and hide features by pressing icon\n at the bottom-left corner", R.drawable.slide_2, getResources().getColor(R.color.half_black)));
        addSlide(AppIntroFragment.newInstance("Menu Items", "Click on the School, Hazard, Toilet, Fountain and Parking buttons to get a more detailed information about your route", R.drawable.slide_3, getResources().getColor(R.color.dark)));
        addSlide(AppIntroFragment.newInstance("What else?", "Click on the info window to get suburb-wise weather and theft information", R.drawable.slide_4, getResources().getColor(R.color.green)));

        //askForPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        //askForPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
