package au.edu.monash.infotech.cycle.appquery.simplecache;

import au.edu.monash.infotech.cycle.shared.Hour;
import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Created by Moody on 5/05/2017.
 */

public class CacheKeyAccidentRoute extends CacheKeyRoute {
    final Hour hour;

    public CacheKeyAccidentRoute(int radius, Unit unit, double startLatitude, double startLongitude, double endLatitude, double endLongitude, Hour hour) {
        super(radius, unit, startLatitude, startLongitude, endLatitude, endLongitude, null);
        this.hour = hour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CacheKeyAccidentRoute c = (CacheKeyAccidentRoute) o;

        return ((this.radius == c.radius) && (this.unit == c.unit) &&
                (this.start.latitude == c.start.latitude) &&
                (this.start.longitude == c.start.longitude)) &&
                (this.end.latitude == c.end.latitude) &&
                (this.end.longitude == c.end.longitude) &&
                (this.hour.getVal() == c.hour.getVal());
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(radius, unit, start.latitude, start.longitude, end.latitude, end.longitude, hour.getVal());
    }
}
