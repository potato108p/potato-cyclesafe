package au.edu.monash.infotech.cycle.common;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by james on 3/5/17.
 */

public class SchoolInfo {
    private List<LatLng> latLngList;
    private List<String> nameList;
    private String status;

    public SchoolInfo() {
    }

    public SchoolInfo(List<LatLng> latLngList, List<String> nameList, String status) {
        this.latLngList = latLngList;
        this.nameList = nameList;
        this.status = status;
    }

    public List<LatLng> getLatLngList() {
        return latLngList;
    }

    public void setLatLngList(List<LatLng> latLngList) {
        this.latLngList = latLngList;
    }

    public List<String> getNameList() {
        return nameList;
    }

    public void setNameList(List<String> nameList) {
        this.nameList = nameList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
