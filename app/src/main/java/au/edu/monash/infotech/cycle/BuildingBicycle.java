package au.edu.monash.infotech.cycle;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * BuildingBicycle bean to store parking building info
 * Created by james on 14/4/17.
 */

public class BuildingBicycle {
    List<String> description;
    List<String> spaceNum;
    List<LatLng> latLngList;
    String status;

    public BuildingBicycle() {
    }

    public BuildingBicycle(List<String> description, List<String> spaceNum, List<LatLng> latLngList, String status) {
        this.description = description;
        this.spaceNum = spaceNum;
        this.latLngList = latLngList;
        this.status = status;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public List<String> getSpaceNum() {
        return spaceNum;
    }

    public void setSpaceNum(List<String> spaceNum) {
        this.spaceNum = spaceNum;
    }

    public List<LatLng> getLatLngList() {
        return latLngList;
    }

    public void setLatLngList(List<LatLng> latLngList) {
        this.latLngList = latLngList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
