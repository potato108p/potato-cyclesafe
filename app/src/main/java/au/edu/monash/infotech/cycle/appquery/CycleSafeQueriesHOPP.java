package au.edu.monash.infotech.cycle.appquery;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.net.ssl.HttpsURLConnection;

import au.edu.monash.infotech.cycle.appquery.simplecache.Cache;
import au.edu.monash.infotech.cycle.appquery.simplecache.CacheKey;
import au.edu.monash.infotech.cycle.appquery.simplecache.CacheKeyAccident;
import au.edu.monash.infotech.cycle.appquery.simplecache.CacheKeyAccidentRoute;
import au.edu.monash.infotech.cycle.appquery.simplecache.CacheKeyRoute;
import au.edu.monash.infotech.cycle.appquery.simplecache.SuburbCache;
import au.edu.monash.infotech.cycle.shared.DayOfWeek;
import au.edu.monash.infotech.cycle.shared.Hour;
import au.edu.monash.infotech.cycle.shared.JSONParams;
import au.edu.monash.infotech.cycle.shared.Month;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.CycleSafeQueries;
import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Created by Moody on 4/04/2017.
 *
 * Client-side implementation of the CycleSafeQueries.
 * This implementation deals with communicating with the backend server.
 */

public class CycleSafeQueriesHOPP implements CycleSafeQueries {
    private final static String BACKEND_URL = "https://3-dot-cyclesafe-163212.appspot.com/";
    //private final static String ACCIDENT_SERVLET_NAME = "accident";
    //private final static String ACCIDENT_ROUTE_SERVLET_NAME = "accidentroute";
    private final static String TOILETS_SERVLET_NAME = "toilet";
    private final static String FOUNTAINS_SERVLET_NAME = "fountains";
    private final static String CYCLERAILS_SERVLET_NAME = "cyclerails";
    private final static String BUILDINGS_WITH_BICYCLE_FACILITIES_SERVLET_NAME = "buildingbicycle";
    private final static String FOUNTAINS_ROUTE_SERVLET_NAME = "fountainsroute";
    private final static String TOILETS_ROUTE_SERVLET_NAME = "toiletroute";
    private final static String CYCLERAILS_ROUTE_SERVLET_NAME = "cyclerailsroute";
    private final static String BUILDINGS_WITH_BICYCLE_FACILITIES_ROUTE_SERVLET_NAME = "buildingbicycleroute";
    private final static String FILTERED_ACCIDENTS_SERVLET_NAME = "filteredaccident";
    private final static String FILTERED_ACCIDENT_ROUTE_SERVLET_NAME = "filteredaccidentroute";
    private final static String SUBURB_THEFT_SERVLET_NAME = "suburbtheft";
    private final static String SCHOOL_SERVLET_NAME = "school";
    private final static String SCHOOL_ROUTE_SERVLET_NAME = "schoolroute";


    /**
     * Converts a list of LatLng objects to a different type, which is supported by the
     * methods of the CycleSafeQueries interface.
     *
     * @param coordList List of coordinates, in LatLng type.
     *
     * @return Same coordinates in a different type.
     */
    public static List<Map<String, Double>> convertLatLngToMap(List<LatLng> coordList) {
        List<Map<String, Double>>  newCoordList = new ArrayList<>();

        for (int i = 0; i < coordList.size(); i = i + 2) {
            LatLng coord = coordList.get(i);
            HashMap<String, Double> newCoord = new HashMap<>();
            newCoord.put(JSONParams.LATITUDE, coord.latitude);
            newCoord.put(JSONParams.LONGITUDE, coord.longitude);
            newCoordList.add(newCoord);
        }

        return newCoordList;
    }


    @Override
    public JsonArray findToiletsAroundMidPoint(double latitude, double longitude, int radius, Unit unit) throws IOException {
        Cache cache = Cache.getCache();
        CacheKey key = new CacheKey(radius, unit, latitude, longitude, TOILETS_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + TOILETS_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        HashMap<String, String> params = buildURLParams(latitude,longitude,radius, unit);
        sendPOSTMessage(params, connection);

        JsonArray response = readResponse(connection);
        cache.put(key, response);

        return response;
    }


    @Override
    public JsonArray findCycleRailsAroundMidPoint(double latitude, double longitude, int radius, Unit unit) throws IOException {
        Cache cache = Cache.getCache();
        CacheKey key = new CacheKey(radius, unit, latitude, longitude, CYCLERAILS_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + CYCLERAILS_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        HashMap<String, String> params = buildURLParams(latitude,longitude,radius,unit);
        sendPOSTMessage(params, connection);

        JsonArray response = readResponse(connection);
        cache.put(key, response);

        return response;
    }


    @Override
    public JsonArray findDrinkingFountainsAroundMidPoint(double latitude, double longitude, int radius, Unit unit) throws IOException {
        Cache cache = Cache.getCache();
        CacheKey key = new CacheKey(radius, unit, latitude, longitude, FOUNTAINS_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + FOUNTAINS_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        HashMap<String, String> params = buildURLParams(latitude, longitude, radius, unit);
        sendPOSTMessage(params, connection);

        JsonArray response = readResponse(connection);
        cache.put(key, response);

        return response;
    }



    @Override
    public JsonArray findBuildingsWithBicycleParkingAroundMidPoint(double latitude, double longitude, int radius, Unit unit) throws Exception {
        Cache cache = Cache.getCache();
        CacheKey key = new CacheKey(radius, unit, latitude, longitude, BUILDINGS_WITH_BICYCLE_FACILITIES_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + BUILDINGS_WITH_BICYCLE_FACILITIES_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        HashMap<String, String> params = buildURLParams(latitude, longitude, radius, unit);
        sendPOSTMessage(params, connection);

        JsonArray response = readResponse(connection);
        cache.put(key, response);

        return response;
    }


    @Override
    public JsonArray findDrinkingFountainsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) throws Exception {
        Cache cache = Cache.getCache();
        int lastIndex = coordinateList.size() - 1;
        double startLatitude = coordinateList.get(0).get(JSONParams.LATITUDE);
        double startLongitude = coordinateList.get(0).get(JSONParams.LONGITUDE);
        double endLatitude = coordinateList.get(lastIndex).get(JSONParams.LATITUDE);
        double endLongitude = coordinateList.get(lastIndex).get(JSONParams.LONGITUDE);
        CacheKeyRoute key = new CacheKeyRoute(radius, unit, startLatitude, startLongitude, endLatitude, endLongitude, FOUNTAINS_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + FOUNTAINS_ROUTE_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        JsonObject req = buildJSONParams(radius, unit, coordinateList);
        sendPOSTMessageWithJson(connection, req);

        JsonArray response = readResponse(connection);
        cache.put(key, response);

        return response;
    }


    @Override
    public JsonArray findToiletsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) throws Exception {
        Cache cache = Cache.getCache();
        int lastIndex = coordinateList.size() - 1;
        double startLatitude = coordinateList.get(0).get(JSONParams.LATITUDE);
        double startLongitude = coordinateList.get(0).get(JSONParams.LONGITUDE);
        double endLatitude = coordinateList.get(lastIndex).get(JSONParams.LATITUDE);
        double endLongitude = coordinateList.get(lastIndex).get(JSONParams.LONGITUDE);
        CacheKeyRoute key = new CacheKeyRoute(radius, unit, startLatitude, startLongitude, endLatitude, endLongitude, TOILETS_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + TOILETS_ROUTE_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        JsonObject req = buildJSONParams(radius, unit, coordinateList);
        sendPOSTMessageWithJson(connection, req);

        JsonArray response = readResponse(connection);
        cache.put(key, response);
        return response;
    }


    @Override
    public JsonArray findCycleRailsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) throws Exception {
        Cache cache = Cache.getCache();
        int lastIndex = coordinateList.size() - 1;
        double startLatitude = coordinateList.get(0).get(JSONParams.LATITUDE);
        double startLongitude = coordinateList.get(0).get(JSONParams.LONGITUDE);
        double endLatitude = coordinateList.get(lastIndex).get(JSONParams.LATITUDE);
        double endLongitude = coordinateList.get(lastIndex).get(JSONParams.LONGITUDE);
        CacheKeyRoute key = new CacheKeyRoute(radius, unit, startLatitude, startLongitude, endLatitude, endLongitude, CYCLERAILS_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + CYCLERAILS_ROUTE_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        JsonObject req = buildJSONParams(radius, unit, coordinateList);
        sendPOSTMessageWithJson(connection, req);

        JsonArray response = readResponse(connection);
        cache.put(key, response);
        return response;
    }


    @Override
    public JsonArray findBuildingsWithBicycleParkingAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) throws Exception {
        Cache cache = Cache.getCache();
        int lastIndex = coordinateList.size() - 1;
        double startLatitude = coordinateList.get(0).get(JSONParams.LATITUDE);
        double startLongitude = coordinateList.get(0).get(JSONParams.LONGITUDE);
        double endLatitude = coordinateList.get(lastIndex).get(JSONParams.LATITUDE);
        double endLongitude = coordinateList.get(lastIndex).get(JSONParams.LONGITUDE);
        CacheKeyRoute key = new CacheKeyRoute(radius, unit, startLatitude, startLongitude, endLatitude, endLongitude, BUILDINGS_WITH_BICYCLE_FACILITIES_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + BUILDINGS_WITH_BICYCLE_FACILITIES_ROUTE_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        JsonObject req = buildJSONParams(radius, unit, coordinateList);
        sendPOSTMessageWithJson(connection, req);

        JsonArray response = readResponse(connection);
        cache.put(key, response);
        return response;
    }


    @Override
    public JsonArray findAccidentNodesAroundMidPoint(double latitude, double longitude, int radius,
                                                     Unit unit, Hour hour) throws Exception {
        Cache cache = Cache.getCache();
        CacheKeyAccident key = new CacheKeyAccident(radius, unit, latitude, longitude, hour);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + FILTERED_ACCIDENTS_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        HashMap<String, String> params = buildURLParams(latitude, longitude, radius, unit, hour);
        sendPOSTMessage(params, connection);

        JsonArray response = readResponse(connection);
        cache.put(key, response);
        return response;
    }


    @Override
    public JsonArray findAccidentNodesAroundRoute(int radius, Unit unit,
                                                  List<Map<String, Double>> coordinateList, Hour hour) throws Exception {
        Cache cache = Cache.getCache();
        int lastIndex = coordinateList.size() - 1;
        double startLatitude = coordinateList.get(0).get(JSONParams.LATITUDE);
        double startLongitude = coordinateList.get(0).get(JSONParams.LONGITUDE);
        double endLatitude = coordinateList.get(lastIndex).get(JSONParams.LATITUDE);
        double endLongitude = coordinateList.get(lastIndex).get(JSONParams.LONGITUDE);
        CacheKeyAccidentRoute key = new CacheKeyAccidentRoute(radius, unit, startLatitude, startLongitude, endLatitude, endLongitude, hour);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + FILTERED_ACCIDENT_ROUTE_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        JsonObject req = buildJSONParams(radius, unit, coordinateList, hour);
        sendPOSTMessageWithJson(connection, req);

        JsonArray response = readResponse(connection);
        cache.put(key, response);
        return response;
    }

    @Override
    public JsonArray getSafetyRatingForSuburb(int postcode) throws Exception {
        SuburbCache suburbCache = SuburbCache.getCache();

        if (suburbCache.containsKey(postcode)) {
            return suburbCache.get(postcode);
        }

        URL url = new URL(BACKEND_URL + SUBURB_THEFT_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        HashMap<String, String> params = buildURLParams(postcode);
        sendPOSTMessage(params, connection);

        JsonArray response = readResponse(connection);
        suburbCache.put(postcode, response);
        return response;
    }

    @Override
    public JsonArray findSchoolsAroundMidPoint(double latitude, double longitude, int radius, Unit unit) throws Exception {
        Cache cache = Cache.getCache();
        CacheKey key = new CacheKey(radius, unit, latitude, longitude, SCHOOL_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + SCHOOL_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        HashMap<String, String> params = buildURLParams(latitude,longitude,radius, unit);
        sendPOSTMessage(params, connection);

        JsonArray response = readResponse(connection);
        cache.put(key, response);
        return response;
    }

    @Override
    public JsonArray findSchoolsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) throws Exception {
        Cache cache = Cache.getCache();
        int lastIndex = coordinateList.size() - 1;
        double startLatitude = coordinateList.get(0).get(JSONParams.LATITUDE);
        double startLongitude = coordinateList.get(0).get(JSONParams.LONGITUDE);
        double endLatitude = coordinateList.get(lastIndex).get(JSONParams.LATITUDE);
        double endLongitude = coordinateList.get(lastIndex).get(JSONParams.LONGITUDE);
        CacheKeyRoute key = new CacheKeyRoute(radius, unit, startLatitude, startLongitude, endLatitude, endLongitude, SCHOOL_SERVLET_NAME);

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        URL url = new URL(BACKEND_URL + SCHOOL_ROUTE_SERVLET_NAME);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        JsonObject req = buildJSONParams(radius, unit, coordinateList);
        sendPOSTMessageWithJson(connection, req);

        JsonArray response = readResponse(connection);
        cache.put(key, response);
        return response;
    }


    private String buildPostDataString(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    private HashMap<String, String> buildURLParams(int postcode){
        HashMap<String, String> params = new HashMap<>();
        params.put(URLParams.POSTCODE, String.valueOf(postcode));

        return params;
    }

    private HashMap<String, String> buildURLParams(double latitude, double longitude, int radius, Unit unit){
        HashMap<String, String> params = new HashMap<>();
        params.put(URLParams.LATITUDE, String.valueOf(latitude));
        params.put(URLParams.LONGITUDE, String.valueOf(longitude));
        params.put(URLParams.RADIUS, String.valueOf(radius));
        params.put(URLParams.UNIT, unit.name());

        return params;
    }


    private HashMap<String, String> buildURLParams(double latitude, double longitude, int radius,
                                                   Unit unit, Hour hour){
        HashMap<String, String> params = new HashMap<>();
        params.put(URLParams.LATITUDE, String.valueOf(latitude));
        params.put(URLParams.LONGITUDE, String.valueOf(longitude));
        params.put(URLParams.RADIUS, String.valueOf(radius));
        params.put(URLParams.UNIT, unit.name());
        params.put(URLParams.HOUR, hour.name());

        return params;
    }


    private JsonObject buildJSONParams(int radius, Unit unit, List<Map<String, Double>> coordinateList){
        //Build param-value pairs
        HashMap<String, String> params = new HashMap<>();
        params.put(JSONParams.RADIUS, String.valueOf(radius));
        params.put(JSONParams.UNIT, unit.name());

        //Build JSON
        JsonObjectBuilder req = Json.createObjectBuilder().add(URLParams.RADIUS, String.valueOf(radius)).add(URLParams.UNIT, unit.name());
        JsonArrayBuilder jsonCoords = Json.createArrayBuilder();
        for (Map<String, Double> coord : coordinateList) {
            jsonCoords.add(Json.createObjectBuilder()
                    .add(URLParams.LATITUDE, String.valueOf(coord.get(URLParams.LATITUDE)))
                    .add(URLParams.LONGITUDE, String.valueOf(coord.get(URLParams.LONGITUDE))));
        }
        req.add(JSONParams.COORDINATE_LIST, jsonCoords.build());

        return req.build();
    }

    private JsonObject buildJSONParams(int radius, Unit unit, List<Map<String, Double>> coordinateList,
                                       Hour hour){
        //Build JSON
        JsonObjectBuilder req = Json.createObjectBuilder().add(URLParams.RADIUS, String.valueOf(radius))
                .add(URLParams.UNIT, unit.name())
                .add(JSONParams.HOUR, hour.name());

        JsonArrayBuilder jsonCoords = Json.createArrayBuilder();
        for (Map<String, Double> coord : coordinateList) {
            jsonCoords.add(Json.createObjectBuilder()
                    .add(URLParams.LATITUDE, String.valueOf(coord.get(URLParams.LATITUDE)))
                    .add(URLParams.LONGITUDE, String.valueOf(coord.get(URLParams.LONGITUDE))));
        }
        req.add(JSONParams.COORDINATE_LIST, jsonCoords.build());

        return req.build();
    }


    private JsonArray readResponse(HttpsURLConnection connection) throws IOException {
        int responseCode = connection.getResponseCode();
        if (responseCode != HttpsURLConnection.HTTP_OK) {
            throw new IOException("Something went wrong with the connection. HTTP OK Response expected, did not receive.");
        }

        JsonReader jsonReader = Json.createReader(connection.getInputStream());
        JsonArray jsonArray = jsonReader.readArray();
        jsonReader.close();

        return jsonArray;
    }


    private void sendPOSTMessage(Map<String,String> params, HttpsURLConnection connection) throws IOException {
        String postParams = buildPostDataString(params);

        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);

        // Execute HTTP Post
        OutputStream outputStream = connection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
        writer.write(postParams);
        writer.flush();
        writer.close();
        outputStream.close();
        connection.connect();
    }


    private void sendPOSTMessageWithJson(HttpsURLConnection connection, JsonObject json) throws IOException {
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);

        // Execute HTTP Post
        OutputStream outputStream = connection.getOutputStream();
        JsonWriter jsonWriter = Json.createWriter(outputStream);
        jsonWriter.writeObject(json);
        jsonWriter.close();
        connection.connect();
    }
}
