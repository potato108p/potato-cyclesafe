package au.edu.monash.infotech.cycle.appquery.simplecache;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.json.JsonArray;

/**
 * Created by Moody on 5/05/2017.
 */

public class Cache extends LinkedHashMap<CacheKey, JsonArray> {
    private final static int SIZE = 12;
    private static Cache cache;

    private Cache() {
        super(SIZE, 0.75f, true);
    }

    public static Cache getCache() {
        if (cache == null) {
            cache = new Cache();
        }

        return cache;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<CacheKey, JsonArray> eldest) {
        return size() > SIZE;
    }
}
