package au.edu.monash.infotech.cycle.appquery.simplecache;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.json.JsonArray;

/**
 * Created by Moody on 5/05/2017.
 */

public class SuburbCache extends LinkedHashMap<Integer, JsonArray> {
    private final static int SIZE = 16;
    private static SuburbCache cache;

    private SuburbCache() {
        super(SIZE, 0.75f, true);
    }

    public static SuburbCache getCache() {
        if (cache == null) {
            cache = new SuburbCache();
        }

        return cache;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<Integer, JsonArray> eldest) {
        return size() > SIZE;
    }
}
