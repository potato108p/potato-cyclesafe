package au.edu.monash.infotech.cycle;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 19/3/17.
 */

public class DecodePolyline {

    public DecodePolyline() {

    }

    /**
     * decode the points return by google direction api
     * @param encoded_points the encoded data of route info
     * @return list of Latlng of route
     */
    public List <LatLng> decodePoints(String encoded_points){
        int index = 0;
        int lat = 0;
        int lng = 0;
        List <LatLng> out = new ArrayList<LatLng>();

        try {
            int shift;
            int result;
            while (index < encoded_points.length()) {
                shift = 0;
                result = 0;
                while (true) {
                    int b = encoded_points.charAt(index++) - '?';
                    result |= ((b & 31) << shift);
                    shift += 5;
                    if (b < 32)
                        break;
                }
                lat += ((result & 1) != 0 ? ~(result >> 1) : result >> 1);

                shift = 0;
                result = 0;
                while (true) {
                    int b = encoded_points.charAt(index++) - '?';
                    result |= ((b & 31) << shift);
                    shift += 5;
                    if (b < 32)
                        break;
                }
                lng += ((result & 1) != 0 ? ~(result >> 1) : result >> 1);
        /* Add the new Lat/Lng to the Array. Convert Location data to Latlng*/
                out.add(new LatLng((lat*10/1E6),(lng*10/1E6)));
            }
            return out;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return out;
    }

}
