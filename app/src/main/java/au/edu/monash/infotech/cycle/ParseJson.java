package au.edu.monash.infotech.cycle;

import android.text.format.DateFormat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.heatmaps.WeightedLatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import au.edu.monash.infotech.cycle.common.SchoolInfo;

/**
 * Parse JSON type data
 * Created by james on 22/3/17.
 */

public class ParseJson {

    public ParseJson() {

    }

    /**
     * Parse direction info from json
     * @param s json string
     * @return direction object
     */
    public DirectionInfo ParseDirectionJson(String s) {
        DirectionInfo directionInfo = new DirectionInfo();
        try {
            JSONObject jsonObject1 = new JSONObject(s);
            //JSONArray jsonArray = jsonObject1.getJSONArray("status");
            String statusStr = jsonObject1.getString("status");
            if (!statusStr.equals("OK")) {
                return null;
            }
            JSONArray jsonArray1 = jsonObject1.getJSONArray("routes");
            JSONObject jsonObject2 = (JSONObject) jsonArray1.get(0);
            JSONArray jsonArray = jsonObject2.getJSONArray("legs");
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONObject jsonObject4 = jsonObject.getJSONObject("distance");
            String distance = jsonObject4.getString("text");
            JSONObject jsonObject5 = jsonObject.getJSONObject("duration");
            String duration = jsonObject5.getString("text");
            JSONObject jsonObject3 = jsonObject2.getJSONObject("overview_polyline");
            String polyline = jsonObject3.getString("points");
            directionInfo = new DirectionInfo(polyline, distance, duration);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return directionInfo;
    }

    /**
     * Parse school info from json
     * @param s json string
     * @return school object
     */
    public List<School> ParsePlaceJson(String s) {
        List<School> schools = new ArrayList<>();
        String statusStr = "";
        try {
            JSONObject jsonObject1 = new JSONObject(s);
            statusStr = jsonObject1.getString("status");
            if (!statusStr.equals("OK")) {
                return schools;
            }
            JSONArray jsonArray1 = jsonObject1.getJSONArray("results");
            for (int i = 0; i < jsonArray1.length(); i++) {
                JSONObject jsonObject2 = (JSONObject) jsonArray1.get(i);
                JSONObject jsonObject3 = jsonObject2.getJSONObject("geometry");
                String name = jsonObject2.getString("name");
                Log.i("Place", name);
                JSONObject jsonObject4 = jsonObject3.getJSONObject("location");
                double lat = Double.parseDouble(jsonObject4.getString("lat"));
                double lng = Double.parseDouble(jsonObject4.getString("lng"));
                School school = new School(lat, lng, name);
                schools.add(school);
            }
            //JSONObject jsonObject2 = (JSONObject) jsonArray1.get(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return schools;
    }

    /**
     * Parse school info from json
     * @param s json string
     * @return school object
     */
    public SchoolInfo ParseSchool(String s) {
        SchoolInfo schoolInfo = new SchoolInfo();
        List<LatLng> latLngList = new ArrayList<>();
        List<String> nameList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length() - 1; i++){
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                double lat = Double.parseDouble(jsonObject.getString("latitude"));
                double lng = Double.parseDouble(jsonObject.getString("longitude"));
                latLngList.add(new LatLng(lat, lng));
                String name = jsonObject.getString("name");
                nameList.add(name);
            }
            JSONObject statusObject = (JSONObject) jsonArray.get(jsonArray.length() - 1);
            String status = statusObject.getString("status-code");
            schoolInfo = new SchoolInfo(latLngList, nameList, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return schoolInfo;
    }

    /**
     * Parse accident info from json
     * @param s json string
     * @return safe info object
     */
    public SafeInfo ParseAccident(String s) {
        SafeInfo safeInfo = new SafeInfo();
        List<WeightedLatLng> latLngList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(s);
            //Log.i("Accident", "array Length: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length() - 1; i++){
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                double lat = Double.parseDouble(jsonObject.getString("latitude"));
                double lng = Double.parseDouble(jsonObject.getString("longitude"));
                LatLng latLng = new LatLng(lat, lng);
                Double num = Double.valueOf(jsonObject.getString("num_accidents"));
                latLngList.add(new WeightedLatLng(latLng, num));
            }
            JSONObject statusObject = (JSONObject) jsonArray.get(jsonArray.length() - 1);
            String status = statusObject.getString("status-code");
            safeInfo = new SafeInfo(latLngList, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return safeInfo;
    }

    /**
     * Parse toilet info from json
     * @param s json string
     * @return toilet holder object contains status and toilet list
     */
    public ToiletHolder ParseToilet(String s) {
        ToiletHolder toiletHolder = new ToiletHolder();
        List<Toilet> toiletList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length() - 1; i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                double lat = Double.parseDouble(jsonObject.getString("latitude"));
                double lng = Double.parseDouble(jsonObject.getString("longitude"));
                LatLng latLng = new LatLng(lat, lng);
                String name = jsonObject.getString("name");
                String id = jsonObject.getString("id");
                String address_note = jsonObject.getString("address_note");
                String male = jsonObject.getString("male");
                String female = jsonObject.getString("female");
                String unisex = jsonObject.getString("unisex");
                String facility_type = jsonObject.getString("facility_type");
                String access_note = jsonObject.getString("access_note");
                String is_open = jsonObject.getString("is_open");
                String openning_hours_schedule = jsonObject.getString("openning_hours_schedule");
                String openning_hours_note = jsonObject.getString("openning_hours_note");
                String baby_change = jsonObject.getString("baby_change");
                String showers = jsonObject.getString("showers");
                String drinking_water = jsonObject.getString("drinking_water");
                String sharps_disposal = jsonObject.getString("sharps_disposal");
                String sanitary_disposal = jsonObject.getString("sanitary_disposal");
                toiletList.add(new Toilet(id, name, address_note, male, female, unisex, facility_type, access_note, is_open, openning_hours_schedule, openning_hours_note, baby_change, showers, drinking_water, sharps_disposal, sanitary_disposal, latLng));
            }
            JSONObject statusObject = (JSONObject) jsonArray.get(jsonArray.length() - 1);
            String status = statusObject.getString("status-code");
            toiletHolder = new ToiletHolder(toiletList, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return toiletHolder;
    }

    /**
     * Parse fountain info from json
     * @param s json string
     * @return fountain object
     */
    public Fountain ParseFountain(String s) {
        Fountain fountain = new Fountain();
        List<LatLng> latLngList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length() - 1; i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                double lat = Double.parseDouble(jsonObject.getString("latitude"));
                double lng = Double.parseDouble(jsonObject.getString("longitude"));
                latLngList.add(new LatLng(lat, lng));
            }
            JSONObject statusObject = (JSONObject) jsonArray.get(jsonArray.length() - 1);
            String status = statusObject.getString("status-code");
            fountain = new Fountain(latLngList, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fountain;
    }

    /**
     * Parse parking info from json
     * @param s json string
     * @return parking building object
     */
    public BuildingBicycle ParseBuildingWithParking(String s) {
        BuildingBicycle buildingBicycle = new BuildingBicycle();
        List<String> descriptionList = new ArrayList<>();
        List<String> spaceNumList = new ArrayList<>();
        List<LatLng> latLngList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length() - 1; i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                double lat = Double.parseDouble(jsonObject.getString("latitude"));
                double lng = Double.parseDouble(jsonObject.getString("longitude"));
                latLngList.add(new LatLng(lat, lng));
                String description = jsonObject.getString("description");
                descriptionList.add(description);
                String spaceNum = jsonObject.getString("num_spaces");
                spaceNumList.add(spaceNum);
            }
            JSONObject statusObject = (JSONObject) jsonArray.get(jsonArray.length() - 1);
            String status = statusObject.getString("status-code");
            buildingBicycle = new BuildingBicycle(descriptionList, spaceNumList, latLngList, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return buildingBicycle;
    }

    /**
     * Parse parking info from json
     * @param s json string
     * @return parking rail object
     */
    public CycleRail PareCycleRail(String s) {
        CycleRail cycleRail = new CycleRail();
        List<String> descriptionList = new ArrayList<>();
        List<LatLng> latLngList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length() - 1; i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                double lat = Double.parseDouble(jsonObject.getString("latitude"));
                double lng = Double.parseDouble(jsonObject.getString("longitude"));
                latLngList.add(new LatLng(lat, lng));
                String description = jsonObject.getString("description");
                descriptionList.add(description);
            }
            JSONObject statusObject = (JSONObject) jsonArray.get(jsonArray.length() - 1);
            String status = statusObject.getString("status-code");
            cycleRail = new CycleRail(descriptionList, latLngList, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cycleRail;
    }

    /**
     * Parse theft info from json
     * @param s json string
     * @return theft object
     */
    public List<String> ParseTheft(String s) {
        List<String> theft = new ArrayList<>();
        String status = null;
        String desc = null;
        try {
            JSONArray jsonArray = new JSONArray(s);
            JSONObject statusObject = (JSONObject) jsonArray.get(jsonArray.length() - 1);
            status = statusObject.getString("status-code");
            if (status.equals("0")) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                desc = jsonObject.getString("safety_desc");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        theft.add(status);
        theft.add(desc);
        return theft;
    }

    /**
     * Parse weather info from json
     * @param s json string
     * @return current weather object
     */
    public Weather ParseWeather(String s) {
        String weatherInfo = "";
        Weather weather = new Weather();
        try {
            JSONObject jsonObject = new JSONObject(s);
            String city = "No data available";
            if (jsonObject.has("name")) {
                city = jsonObject.getString("name");
            }
            Log.i("Weather", "City: " + city);

            String visibility = "";
            if (jsonObject.has("visibility")) {
                visibility = jsonObject.getString("visibility");
            }
            Log.i("Weather", "visibility: " + visibility);

            String time = "";
            if (jsonObject.has("dt")) {
                time = jsonObject.getString("dt");
            }
            Log.i("Weather", "time: " + time);

            String description = "";
            String url = "03d";
            if (jsonObject.has("weather")) {
                JSONArray jsonArray = jsonObject.getJSONArray("weather");
                JSONObject jsonObject1 = (JSONObject) jsonArray.get(0);
                if (jsonObject1.has("description")) {
                    description = jsonObject1.getString("description");
                }
                Log.i("Weather", "description: " + description);
                if (jsonObject1.has("icon")) {
                    url = jsonObject1.getString("icon");
                }
                Log.i("Weather", "url: " + url);
            }

            String temp = "";
            String pressure = "0";
            String humidity = "";
            if (jsonObject.has("main")) {
                JSONObject jsonObject2 = jsonObject.getJSONObject("main");
                if (jsonObject2.has("temp")) {
                    temp = jsonObject2.getString("temp");
                }
                Log.i("Weather", "temp: " + temp);
                if (jsonObject2.has("pressure")) {
                    pressure = jsonObject2.getString("pressure");
                }
                Log.i("Weather", "pressure: " + pressure);
                if (jsonObject2.has("humidity")) {
                    humidity = jsonObject2.getString("humidity");
                }
                Log.i("Weather", "humidity: " + humidity);
            }

            String speed = "";
            String deg = "";
            if (jsonObject.has("wind")) {
                JSONObject jsonObject3 = jsonObject.getJSONObject("wind");
                if (jsonObject3.has("speed")) {
                    speed = jsonObject3.getString("speed");
                }
                Log.i("Weather", "speed: " + speed);
                if (jsonObject3.has("deg")) {
                    deg = jsonObject3.getString("deg");
                }
                Log.i("Weather", "deg: " + deg);
            }

            String cloud = "";
            if (jsonObject.has("clouds")) {
                JSONObject jsonObject4 = jsonObject.getJSONObject("clouds");
                if (jsonObject4.has("all")) {
                    cloud = jsonObject4.getString("all");
                }
                Log.i("Weather", "cloud: " + cloud);
            }

            weather.setDescription(description);
            weather.setTemp(temp);
            weather.setPressure(pressure);
            weather.setHumidity(humidity);
            weather.setWindSpeed(speed);
            weather.setWindDirection(deg);
            weather.setCloud(cloud);
            weather.setVisibility(visibility);
            weather.setTime(time);
            weather.setUrl(url);
            weather.setCity(city);
            weatherInfo += temp;
            //Log.i("Weather", description + "; " + temp+ "; " +pressure+ "; " +humidity+ "; " +speed+ "; " +deg+ "; " +cloud+ "; " +visibility+ "; " +time);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weather;
    }

    /**
     * Parse weather info from json
     * @param s json string
     * @return weather forecast object
     */
    public List<WeatherForecast> ParseWeatherForecast(String s) {
        List<WeatherForecast> weatherForecastList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list");
            for (int i = 0; i < 8; i++) {
                JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                //String time = jsonObject1.getString("dt");
                String time = "No data available";
                if (jsonObject1.has("dt")) {
                    long timestamp = Long.parseLong(jsonObject1.getString("dt"));
                    time = getDate(timestamp);
                }
                String temp = "-";
                if (jsonObject1.has("main")) {
                    JSONObject jsonObject2 = jsonObject1.getJSONObject("main");
                    //String temp = jsonObject2.getString("temp");
                    if (jsonObject2.has("temp")) {
                        temp = String.valueOf((int) (Double.parseDouble(jsonObject2.getString("temp")) - 273.15));
                    }
                }
                String windSpeed = "-";
                String windDir = "-";
                if (jsonObject1.has("wind")) {
                    JSONObject jsonObject3 = jsonObject1.getJSONObject("wind");
                    if (jsonObject3.has("speed")) {
                        windSpeed = jsonObject3.getString("speed");
                    }
                    if (jsonObject3.has("deg")) {
                        windDir = directionChange(Double.parseDouble(jsonObject3.getString("deg")));
                    }
                }
                String description = "No data available";
                String icon = "03d";
                if (jsonObject1.has("weather")) {
                    JSONArray jsonArray1 = jsonObject1.getJSONArray("weather");
                    JSONObject jsonObject4 = jsonArray1.getJSONObject(0);
                    if (jsonObject4.has("description")) {
                        description = jsonObject4.getString("description");
                        description = description.substring(0,1).toUpperCase()+description.substring(1);
                    }
                    if (jsonObject4.has("icon")) {
                        icon = jsonObject4.getString("icon");
                    }
                }
                WeatherForecast weatherForecast = new WeatherForecast(icon, time, temp, windSpeed, windDir, description);
                weatherForecastList.add(weatherForecast);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weatherForecastList;
    }

    /**
     * convert time stamp to string
     * @param time time stamp
     * @return formated time string
     */
    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time*1000);
        String dateStr = DateFormat.format("E h a", cal).toString();
        return dateStr;
    }

    /**
     * convert direction value to string
     * @param d double direction value
     * @return direction string
     */
    private String directionChange (double d) {
        String direction;
        if (d >= 22.5 && d < 67.5) {
            direction = "NE";
        } else if (d < 112.5) {
            direction = "E";
        } else if (d < 157.5) {
            direction = "SE";
        } else if (d < 202.5) {
            direction = "S";
        } else if (d < 247.5) {
            direction = "SW";
        } else if (d < 292.5) {
            direction = "W";
        } else if (d < 337.5) {
            direction = "NW";
        } else {
            direction = "N";
        }
        return direction;
    }
}
