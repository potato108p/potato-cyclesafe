package au.edu.monash.infotech.cycle;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Fountain bean, store fountain info
 * Created by james on 12/4/17.
 */

public class Fountain {
    private List<LatLng> latLngList;
    private String status;

    public Fountain() {
    }

    public Fountain(List<LatLng> latLngList, String status) {
        this.latLngList = latLngList;
        this.status = status;
    }

    public List<LatLng> getLatLngList() {
        return latLngList;
    }

    public void setLatLngList(List<LatLng> latLngList) {
        this.latLngList = latLngList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
