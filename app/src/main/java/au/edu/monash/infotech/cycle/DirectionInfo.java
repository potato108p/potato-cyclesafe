package au.edu.monash.infotech.cycle;

/**
 * Bean stores direction info
 * Created by james on 2/5/17.
 */

public class DirectionInfo {
    String polyline;
    String distance;
    String duration;

    public DirectionInfo() {
    }

    public DirectionInfo(String polyline, String distance, String duration) {
        this.polyline = polyline;
        this.distance = distance;
        this.duration = duration;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

}
