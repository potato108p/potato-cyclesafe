package au.edu.monash.infotech.cycle.appquery.simplecache;

import com.google.android.gms.maps.model.LatLng;
import java.io.Serializable;
import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Created by Moody on 5/05/2017.
 */

public class CacheKey {
    final int radius;
    final Unit unit;
    final LatLng start;
    final String servletName;

    public CacheKey(int radius, Unit unit, double latitude, double longitude, String servletName) {
        this.radius = radius;
        this.unit = unit;
        this.start = new LatLng(latitude, longitude);
        this.servletName = servletName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CacheKey c = (CacheKey) o;

        return ((this.radius == c.radius) && (this.unit == c.unit) &&
                (this.start.latitude == c.start.latitude) &&
                (this.start.longitude == c.start.longitude) &&
                (this.servletName.equals(c.servletName)));
    }


    @Override
    public int hashCode() {
        return java.util.Objects.hash(radius, unit, start.latitude, start.longitude, servletName);
    }
}
