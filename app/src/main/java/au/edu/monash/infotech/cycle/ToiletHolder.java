package au.edu.monash.infotech.cycle;

import java.util.List;

/**
 * Store toilet list and search result status code
 * Created by james on 3/5/17.
 */

public class ToiletHolder {
    private List<Toilet> toiletList;
    private String status;

    public ToiletHolder() {
    }

    public ToiletHolder(List<Toilet> toiletList, String status) {
        this.toiletList = toiletList;
        this.status = status;
    }

    public List<Toilet> getToiletList() {
        return toiletList;
    }

    public void setToiletList(List<Toilet> toiletList) {
        this.toiletList = toiletList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
