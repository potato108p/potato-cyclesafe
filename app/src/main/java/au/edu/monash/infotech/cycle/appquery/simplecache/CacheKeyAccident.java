package au.edu.monash.infotech.cycle.appquery.simplecache;

import au.edu.monash.infotech.cycle.shared.Hour;
import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Created by Moody on 5/05/2017.
 */

public class CacheKeyAccident extends CacheKey {
    final Hour hour;

    public CacheKeyAccident(int radius, Unit unit, double latitude, double longitude, Hour hour) {
        super(radius, unit, latitude, longitude, null);
        this.hour = hour;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CacheKeyAccident c = (CacheKeyAccident) o;

        return ((this.radius == c.radius) && (this.unit == c.unit) &&
                (this.start.latitude == c.start.latitude) &&
                (this.start.longitude == c.start.longitude) &&
                (this.hour.getVal() == c.hour.getVal()));
    }


    @Override
    public int hashCode() {
        return java.util.Objects.hash(radius, unit, start.latitude, start.longitude, hour.getVal());
    }
}
