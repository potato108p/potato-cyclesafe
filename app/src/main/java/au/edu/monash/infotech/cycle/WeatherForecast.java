package au.edu.monash.infotech.cycle;

/**
 * Weather bean, store weather info
 * Created by james on 15/4/17.
 */

public class WeatherForecast {

    private String icon;
    private String time;
    private String temp;
    private String windSpeed;
    private String windDir;
    private String description;

    public WeatherForecast() {
    }

    public WeatherForecast(String icon, String time, String temp, String windSpeed, String windDir, String description) {
        this.icon = icon;
        this.time = time;
        this.temp = temp;
        this.windSpeed = windSpeed;
        this.windDir = windDir;
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getWindDir() {
        return windDir;
    }

    public void setWindDir(String windDir) {
        this.windDir = windDir;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
