package au.edu.monash.infotech.cycle;

import android.app.Dialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.format.DateFormat;
import android.text.style.LocaleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Weather fragment, modal bottom sheet
 * Created by james on 8/4/17.
 */

public class WeatherFragment extends BottomSheetDialogFragment {

    private ListView lvWeather;
    private AVLoadingIndicatorView avLoadingIndicatorView;
    //public ImageView imIcon;
    //public TextView tvCity,tvTemp,tvDescription,tvWind

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.weather_fragment, container, false);
        //TextView textView = (TextView) v.findViewById(R.id.tvWeather);
        //TextView tvTitle = (TextView) v.findViewById(R.id.tvWeatherTitle);
        ImageView imIcon = (ImageView) v.findViewById(R.id.imIcon);
        TextView tvCity = (TextView) v.findViewById(R.id.tvCity);
        TextView tvTemp = (TextView) v.findViewById(R.id.tvTemp);
        TextView tvDescription = (TextView) v.findViewById(R.id.tvDescription);
        TextView tvWind = (TextView) v.findViewById(R.id.tvWind);
        TextView tvHumidity = (TextView) v.findViewById(R.id.tvHumidity);
        TextView tvCloud = (TextView) v.findViewById(R.id.tvCloud);
        TextView tvVisibility = (TextView) v.findViewById(R.id.tvVisibility);
        TextView tvTheft = (TextView) v.findViewById(R.id.tvTheft);
        avLoadingIndicatorView = (AVLoadingIndicatorView) v.findViewById(R.id.aviFragment);
        lvWeather = (ListView) v.findViewById(R.id.lvWeather);

        Bundle bundle = getArguments();
        Weather weather = bundle.getParcelable("Object");
        String forecastUrl = bundle.getString("Forecast");
        String theft = bundle.getString("Theft");
        tvTheft.setText(theft);
        if (theft.equals("Low bicycle theft")) {
            tvTheft.setTextColor(getResources().getColor(R.color.green));
        } else if (theft.equals("Moderate bicycle theft")) {
            tvTheft.setTextColor(Color.parseColor("#FF9800"));
        } else if (theft.equals("High bicycle theft")) {
            tvTheft.setTextColor(getResources().getColor(R.color.red));
        }
        //transfer temperature
        if (weather.getTemp().isEmpty()) {
            tvTemp.setText("--");
        } else {
            int temp = (int) (Double.parseDouble(weather.getTemp()) - 273.15);
            tvTemp.setText(temp + "\u2103");
        }
        //transfer first char to upper case
        if (weather.getDescription().isEmpty()) {
            tvDescription.setText("No data available");
        } else {
            String description = weather.getDescription();
            description = description.substring(0,1).toUpperCase()+description.substring(1);
            tvDescription.setText(description);
        }
        //transfer direction
        if (!weather.getWindDirection().isEmpty() && !weather.getWindSpeed().isEmpty()) {
            double directionDouble = Double.parseDouble(weather.getWindDirection());
            String direction = directionChange(directionDouble);
            tvWind.setText(weather.getWindSpeed() + " m/s - " + direction);
        } else if (weather.getWindSpeed().isEmpty()) {
            double directionDouble = Double.parseDouble(weather.getWindDirection());
            String direction = directionChange(directionDouble);
            tvWind.setText("--"+ " m/s - " + direction);
        } else if (weather.getWindDirection().isEmpty()){
            tvWind.setText(weather.getWindSpeed() + " m/s");
        }

        //transfer timestamp to correct time format
        long timestamp = Long.parseLong(weather.getTime());
        String time = getDate(timestamp);
        //set weather icon
        String url = weather.getUrl();
        Ion.with(imIcon).load("http://openweathermap.org/img/w/" + url + ".png");

        tvCity.setText(weather.getCity());
        // check whether the suburb has humidity value
        if (weather.getHumidity().isEmpty()) {
            tvHumidity.setText("No data available");
        } else {
            tvHumidity.setText(weather.getHumidity() + "%");
        }
        // check whether the suburb has cloud value
        if (weather.getCloud().isEmpty()) {
            tvCloud.setText("No data available");
        } else {
            tvCloud.setText(weather.getCloud() + "%");
        }

        // check whether the suburb has visibility value
        if (weather.getVisibility().isEmpty()) {
            tvVisibility.setText("No data available");
        } else {
            tvVisibility.setText(weather.getVisibility() + " m");
        }


        new WeatherForecastAsyncTask().execute(forecastUrl);

//        textView.setText("Temperature: " + temp + "\u2103"
//                + "\nDescription: " + description
//                + "\nHumidity: " + weather.getHumidity() + "%"
//                + "\nWind Speed: " + weather.getWindSpeed() + " meter/sec"
//                + "\nWind Direction: " + direction
//                + "\nCloud: " + weather.getCloud() + "%"
//                + "\nVisibility: " + weather.getVisibility() + " meter"
//                + "\nUpdate Time: " + time + "(UTC)");
        return v;
    }

    /**
     * convert direction value to string
     * @param d double direction value
     * @return direction string
     */
    public String directionChange (double d) {
        String direction = null;
        if (d >= 22.5 && d < 67.5) {
            direction = "NE";
        } else if (d < 112.5) {
            direction = "E";
        } else if (d < 157.5) {
            direction = "SE";
        } else if (d < 202.5) {
            direction = "S";
        } else if (d < 247.5) {
            direction = "SW";
        } else if (d < 292.5) {
            direction = "W";
        } else if (d < 337.5) {
            direction = "NW";
        } else {
            direction = "N";
        }
        return direction;
    }

    /**
     * convert time stamp to string
     * @param time time stamp
     * @return formated time string
     */
    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time*1000);
        String dateStr = DateFormat.format("dd-MM-yyyy HH:mm:ss", cal).toString();
        return dateStr;
    }

    /**
     * AsyncTask for parsing weather info and set the weather forecast adapter
     */
    class WeatherForecastAsyncTask extends AsyncTask<String, Void, List<WeatherForecast>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avLoadingIndicatorView.smoothToShow();
        }

        @Override
        protected List<WeatherForecast> doInBackground(String... params) {
            HttpHandler sh = new HttpHandler();
            String jsonStr = sh.makeServiceCall(params[0]);
            ParseJson pj = new ParseJson();
            List<WeatherForecast> weatherForecastList = pj.ParseWeatherForecast(jsonStr);
            return weatherForecastList;
        }

        @Override
        protected void onPostExecute(List<WeatherForecast> weatherForecastList) {
            super.onPostExecute(weatherForecastList);
            WeatherAdapter adapter = new WeatherAdapter(getActivity(), weatherForecastList);
            lvWeather.setAdapter(adapter);
            avLoadingIndicatorView.smoothToHide();
        }
    }

}