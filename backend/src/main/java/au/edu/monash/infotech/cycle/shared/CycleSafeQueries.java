package au.edu.monash.infotech.cycle.shared;

import java.util.List;
import java.util.Map;
import javax.json.JsonArray;

/**
 * Created by Moody on 4/04/2017.
 */

public interface CycleSafeQueries {
    /**
     * Finds all the toilets, within the radius of a given point.
     *
     * @param latitude Your latitude
     * @param longitude Your longitude
     * @param radius Your search distance
     * @param unit Unit of your radius
     *
     * @return JsonArray of all the toilets found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findToiletsAroundMidPoint(double latitude, double longitude, int radius,
                                        Unit unit) throws Exception;

    /**
     * Finds all the bicycle rails for securing your bicycle, within the radius of a given point.
     *
     * @param latitude Your latitude
     * @param longitude Your longitude
     * @param radius Your search distance
     * @param unit Unit of your radius
     *
     * @return JsonArray of all the bicycle rails found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findCycleRailsAroundMidPoint(double latitude, double longitude, int radius,
                                           Unit unit) throws Exception;

    /**
     * Finds all the drinking fountains, within the radius of a given point.
     *
     * @param latitude Your latitude
     * @param longitude Your longitude
     * @param radius Your search distance
     * @param unit Unit of your radius
     *
     * @return JsonArray of all the drinking fountains found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findDrinkingFountainsAroundMidPoint(double latitude, double longitude, int radius,
                                                  Unit unit) throws Exception;


    /**
     * Find buildings that have parking facilities around a given point.
     *
     * @param latitude Your latitude
     * @param longitude Your longitude
     * @param radius The search distancee
     * @param unit Unit of your radius
     *
     * @return JsonArray of all buildings with bicycle parking facilities found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findBuildingsWithBicycleParkingAroundMidPoint(double latitude, double longitude,
                                                            int radius, Unit unit) throws Exception;


    /**
     * Finds all drinking fountains, within the radius of a list of given coordinates.
     *
     * @param radius The search distance
     * @param unit Unit of your radius
     * @param coordinateList List of latitude, longitude coordinates.
     *
     * @return JsonArray of all the drinking fountains found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findDrinkingFountainsAroundRoute(int radius, Unit unit,
                                               List<Map<String, Double>> coordinateList) throws Exception;


    /**
     * Finds all public toilets, within the radius of a list of given coordinates.
     *
     * @param radius The search distance
     * @param unit Unit of your radius
     * @param coordinateList List of latitude, longitude coordinates.
     *
     * @return JsonArray of all the public toilets found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findToiletsAroundRoute(int radius, Unit unit,
                                               List<Map<String, Double>> coordinateList) throws Exception;

    /**
     * Finds all cycle rails for securing your bicycle, within the radius of a list of given coordinates.
     *
     * @param radius The search distance
     * @param unit Unit of your radius
     * @param coordinateList List of latitude, longitude coordinates.
     *
     * @return JsonArray of all the drinking fountains found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findCycleRailsAroundRoute(int radius, Unit unit,
                                     List<Map<String, Double>> coordinateList) throws Exception;


    /**
     * Finds all buildings with bicycle parking facilities, within the radius of a list of given coordinates.
     *
     * @param radius The search distance
     * @param unit Unit of your radius
     * @param coordinateList List of latitude, longitude coordinates.
     *
     * @return JsonArray of all the buildings with bicycle parking found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     * query was successful or not.
     */
    JsonArray findBuildingsWithBicycleParkingAroundRoute(int radius, Unit unit,
                                                         List<Map<String, Double>> coordinateList) throws Exception;

    /**
     * Finds all the locations (nodes) with recorded bicycle accidents, within the radius of a given point.
     *
     * @param latitude Your latitude
     * @param longitude Your longitude
     * @param radius The search distance
     * @param unit Unit of your radius
     * @param hour Hour of the day
     *
     * @return JsonArray of all the accident nodes found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     */
    JsonArray findAccidentNodesAroundMidPoint(final double latitude, final double longitude,
                                                     final int radius, final Unit unit, Hour hour)
            throws Exception;

    /**
     * Finds all the locations (nodes) with recorded bicycle accidents, within the radius of a given point.
     *
     * @param radius The search distance
     * @param unit Unit of your radius
     * @param coordinateList List of latitude, longitude coordinates.
     * @param hour Hour of the day
     *
     * @return JsonArray of all the accident nodes found. A status-code field is included: 0 is success, everything else is failure.
     *
     * @throws Exception Thrown when there is a failure in communicating with the server. No exceptions are thrown for
     * for invalid paramaters or values - see the status-code in the returned JsonArray to confirm the
     */
    JsonArray findAccidentNodesAroundRoute(int radius, Unit unit,
                                           List<Map<String, Double>> coordinateList, Hour hour)
            throws Exception;

    /**
     * Finds the safety rating for a given postcode, in terms of bicycle theft.
     *
     * @param postcode - The postcode you want the safety rating for.
     *
     * @return JsonArray with the details of the suburb in terms of bicycle thefts.
     * @throws Exception
     */
    JsonArray getSafetyRatingForSuburb(int postcode) throws Exception;


    JsonArray findSchoolsAroundMidPoint(final double latitude, final double longitude,
                                       final int radius, final Unit unit) throws Exception;


    JsonArray findSchoolsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList)
            throws Exception;
}
