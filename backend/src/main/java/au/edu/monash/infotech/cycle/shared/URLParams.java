package au.edu.monash.infotech.cycle.shared;

/**
 * Created by Moody on 4/04/2017.
 *
 * Parameters for supported URL parameter names.
 */

public class URLParams {
    private URLParams() {
    }

    public final static String LATITUDE = "la";
    public final static String LONGITUDE = "lo";
    public final static String RADIUS = "r";
    public final static String UNIT = "u";
    public final static String HOUR = "h";
    public final static String DAY = "d";
    public final static String MONTH = "m";
    public final static String POSTCODE = "p";
}
