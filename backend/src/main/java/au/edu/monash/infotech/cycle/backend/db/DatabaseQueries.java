package au.edu.monash.infotech.cycle.backend.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import au.edu.monash.infotech.cycle.shared.CycleSafeQueries;
import au.edu.monash.infotech.cycle.shared.Hour;
import au.edu.monash.infotech.cycle.shared.JSONParams;
import au.edu.monash.infotech.cycle.shared.StatusCode;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Created by Moody on 4/04/2017.
 * Server-side implementation of the CycleSafeQueries interface.
 * This implementation deals with the database calls to satisfy the queries.
 */

public class DatabaseQueries implements CycleSafeQueries {
    private final static boolean DEBUG = false;

    private final static String USER = "moody";
    private final static String PASSWORD = "Dogbrothel12";
    private final static String INSTANCE_NAME = "cyclesafe-163212:asia-east1:cyclesafe-potato/";
    private final static String DB_NAME = "cyclesafe";
    private final static String JDBC_URL = "jdbc:google:mysql://" + INSTANCE_NAME + DB_NAME;
    private static DatabaseQueries dbQueries;
    private SQLManager sqlManager; /* Manages the database connection pool */


    /* Constants required for filtering accidents by time period */
    private static final int[] TIME_PERIOD = {4, 4, 4, 4, 4, 4, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4};

    private DatabaseQueries() {
        sqlManager = new SQLManager("com.mysql.jdbc.GoogleDriver", true,
                JDBC_URL, USER, PASSWORD);
    }


    public static synchronized DatabaseQueries getDatabaseQueriesInstance() {
        if (DatabaseQueries.dbQueries != null) {
            return DatabaseQueries.dbQueries;
        } else {
            DatabaseQueries.dbQueries = new DatabaseQueries();
            return DatabaseQueries.dbQueries;
        }
    }


    public JsonArray findAccidentNodesAroundMidPoint(double latitude, double longitude, int radius, Unit unit) {
        String[] fieldNamesInOrder = {DBTableInfo.ACCIDENT_NODE_ID_NAME, DBTableInfo.ACCIDENT_NODE_LATITUDE_NAME,
                DBTableInfo.ACCIDENT_NODE_LONGITUDE_NAME, DBTableInfo.ACCIDENT_NODE_POSTCODE_NAME};
        return findCoordinatesAroundPoint(latitude, longitude, radius, unit,
                DBTableInfo.ACCIDENT_NODE_TABLE_NAME, fieldNamesInOrder);
    }


    @Override
    public JsonArray findCycleRailsAroundMidPoint(double latitude, double longitude, int radius, Unit unit) {
        String[] fieldNamesInOrder = {DBTableInfo.CYCLERAILS_DESC_NAME, DBTableInfo.CYCLERAILS_LONGITUDE_NAME,
                DBTableInfo.CYCLERAILS_LATITUDE_NAME, DBTableInfo.CYCLERAILS_ID_NAME};
        return findCoordinatesAroundPoint(latitude, longitude, radius, unit, DBTableInfo.CYCLERAILS_TABLE_NAME,
                fieldNamesInOrder);
    }


    @Override
    public JsonArray findDrinkingFountainsAroundMidPoint(double latitude, double longitude, int radius, Unit unit) {
        String[] fieldNamesInOrder = {DBTableInfo.FOUNTAINS_LATITUDE_NAME, DBTableInfo.FOUNTAINS_LONGITUDE_NAME, DBTableInfo.FOUNTAINS_ID_NAME};
        return findCoordinatesAroundPoint(latitude, longitude, radius, unit, DBTableInfo.FOUNTAINS_TABLE_NAME,
                fieldNamesInOrder);
    }


    @Override
    public JsonArray findBuildingsWithBicycleParkingAroundMidPoint(double latitude, double longitude, int radius, Unit unit) {
        String[] fieldNamesInOrder = {DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_PROPERTY_ID_NAME,
                DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_DESCRIPTION_NAME, DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_NUM_SPACES_NAME,
                DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_LATITUDE_NAME, DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_LONGITUDE_NAME};
        return findCoordinatesAroundPoint(latitude, longitude, radius, unit,
                DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_TABLE_NAME, fieldNamesInOrder);
    }

    @Override
    public JsonArray findDrinkingFountainsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) {
        String tableName = DBTableInfo.FOUNTAINS_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.FOUNTAINS_LATITUDE_NAME, DBTableInfo.FOUNTAINS_LONGITUDE_NAME, DBTableInfo.FOUNTAINS_ID_NAME};

        return findPlacesAroundARoute(radius, unit, coordinateList, tableName, fieldNamesInOrder);
    }


    @Override
    public JsonArray findCycleRailsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) {
        String tableName = DBTableInfo.CYCLERAILS_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.CYCLERAILS_DESC_NAME, DBTableInfo.CYCLERAILS_LONGITUDE_NAME,
                DBTableInfo.CYCLERAILS_LATITUDE_NAME, DBTableInfo.CYCLERAILS_ID_NAME};

        return findPlacesAroundARoute(radius, unit, coordinateList, tableName, fieldNamesInOrder);
    }


    @Override
    public JsonArray findBuildingsWithBicycleParkingAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList){
        String tableName = DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_PROPERTY_ID_NAME,
                DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_DESCRIPTION_NAME, DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_NUM_SPACES_NAME,
                DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_LATITUDE_NAME, DBTableInfo.BUILDINGS_WITH_BICYCLE_FAC_LONGITUDE_NAME};

        return findPlacesAroundARoute(radius, unit, coordinateList, tableName, fieldNamesInOrder);
    }



    @Override
    public JsonArray findAccidentNodesAroundMidPoint(final double latitude, final double longitude,
                                                 final int radius, final Unit unit, Hour hour){
        JsonArray nodesAroundPoint = findAccidentNodesAroundMidPoint(latitude, longitude, radius, unit);
        return filterNodesByTimePeriod(nodesAroundPoint, hour);
    }


    /*
     * Given a list of nodes, and date/time, only return the nodes whose accidents have taken place
     * at that time.
     */
    private JsonArray filterNodesByTimePeriod(JsonArray nodeList, Hour hour) {
        int timePeriodId = TIME_PERIOD[hour.getVal()];

        String queryString = String.format("SELECT %s, SUM(%s) FROM %s WHERE %s = ? GROUP BY %s;",
                DWTableInfo.ACCIDENT_FACT_NODE_ID_NAME, DWTableInfo.ACCIDENT_FACT_NUM_ACCIDENTS_NAME,
                DWTableInfo.ACCIDENT_FACT_TABLE_NAME,
                DWTableInfo.ACCIDENT_FACT_TIMEPERIOD_ID_NAME, DWTableInfo.ACCIDENT_FACT_NODE_ID_NAME);

        HashMap<String, Integer> nodesInTimeDatePeriod = new HashMap<>();

        try (Connection conn = sqlManager.getConnection()) {
            PreparedStatement query = conn.prepareStatement(queryString);
            query.setInt(1, timePeriodId);

            ResultSet rs = query.executeQuery();

            while (rs.next()) {
                nodesInTimeDatePeriod.put(rs.getString(1), rs.getInt(2));
            }
        } catch (SQLException sqlE) {
            return Json.createArrayBuilder().add(StatusCode.error(StatusCode.SQL_ERROR, sqlE.getMessage())).build();
        } catch (Exception e) {
            return Json.createArrayBuilder().add(StatusCode.error(StatusCode.UNKNOWN_ERROR, e.getMessage())).build();
        }

        JsonArrayBuilder timeFilteredNodes = Json.createArrayBuilder();

        int count = 0;
        for (JsonValue jsonValue: nodeList) {
            JsonObject node = (JsonObject) jsonValue;
            String val = node.getString(DBTableInfo.ACCIDENT_NODE_ID_NAME, "sentinel");

            if (val != "sentinel" && nodesInTimeDatePeriod.containsKey(val)) {
                    JsonObjectBuilder temp = Json.createObjectBuilder();
                    for (Map.Entry<String, JsonValue> entry: node.entrySet()) {
                        temp.add(entry.getKey(), entry.getValue());
                    }
                    temp.add(DWTableInfo.ACCIDENT_FACT_NUM_ACCIDENTS_NAME, nodesInTimeDatePeriod.get(val));
                    timeFilteredNodes.add(temp);
                    count++;
            }
        }

        timeFilteredNodes.add(StatusCode.success(count));
        if (DEBUG)
            timeFilteredNodes.add(numResultsInJson(count));

        return timeFilteredNodes.build();
    }


    @Override
    public JsonArray findAccidentNodesAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList,
                                                  Hour hour) {
        JsonArray nodesAroundPoint = findAccidentNodesAroundRoute(radius, unit, coordinateList);
        return filterNodesByTimePeriod(nodesAroundPoint, hour);
    }


    @Override
    public JsonArray getSafetyRatingForSuburb(int postcode)  {
        JsonArrayBuilder response = Json.createArrayBuilder();

        try (Connection conn = sqlManager.getConnection()) {
            String queryStr = String.format("SELECT * FROM %s WHERE %s = ?;",
                    DBTableInfo.SUBURB_THEFT_TABLE_NAME, DBTableInfo.SUBURB_THEFT_POSTCODE_NAME);
            PreparedStatement query = conn.prepareStatement(queryStr);
            query.setInt(1, postcode);
            ResultSet rs = query.executeQuery();
            int count = 0;

            while (rs.next()) {
                count++;
                response.add(Json.createObjectBuilder().add(
                        DBTableInfo.SUBURB_THEFT_POSTCODE_NAME,
                        rs.getString(DBTableInfo.SUBURB_THEFT_POSTCODE_POS))
                        .add(
                                DBTableInfo.SUBURB_THEFT_NUM_OFFENSES_NAME,
                                rs.getString(DBTableInfo.SUBURB_THEFT_NUM_OFFENSES_POS))
                        .add(
                                DBTableInfo.SUBURB_THEFT_SAFETY_DESC_NAME,
                                rs.getString(DBTableInfo.SUBURB_THEFT_SAFETY_DESC_POS)
                        )
                );
            }

            if (count > 1) {
                response.add(StatusCode.error(StatusCode.UNKNOWN_ERROR, "Query returned multiple suburbs. Error on server query."));
                return response.build();
            } else if (count < 1) {
                response.add(StatusCode.error(StatusCode.INVALID_PARAM_VAL, "The given postcode does not exist."));
                return response.build();
            } else {
                response.add(StatusCode.success(count));
                return response.build();
            }
        } catch (SQLException sqlE) {
            return response.add(StatusCode.error(StatusCode.SQL_ERROR, sqlE.getMessage())).build();
        } catch (Exception e) {
            return response.add(StatusCode.error(StatusCode.UNKNOWN_ERROR, e.getMessage())).build();
        }
    }


    @Override
    public JsonArray findSchoolsAroundMidPoint(double latitude, double longitude, int radius, Unit unit)  {
        String tableName = DBTableInfo.SCHOOL_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.SCHOOL_ID_NAME,
                DBTableInfo.SCHOOL_NAME_NAME,
                DBTableInfo.SCHOOL_LONGITUDE_NAME,
                DBTableInfo.SCHOOL_LATITUDE_NAME};

        return findCoordinatesAroundPoint(latitude, longitude, radius, unit, tableName,
                fieldNamesInOrder);
    }


    @Override
    public JsonArray findSchoolsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) {
        String tableName = DBTableInfo.SCHOOL_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.SCHOOL_ID_NAME,
                DBTableInfo.SCHOOL_NAME_NAME,
                DBTableInfo.SCHOOL_LONGITUDE_NAME,
                DBTableInfo.SCHOOL_LATITUDE_NAME};

        return findPlacesAroundARoute(radius, unit, coordinateList, tableName, fieldNamesInOrder);
    }


    @Override
    public JsonArray findToiletsAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) {
        String tableName = DBTableInfo.TOILET_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.TOILET_ID_NAME,
                DBTableInfo.TOILET_NAME_NAME,
                DBTableInfo.TOILET_ADDRESSNOTE_NAME,
                DBTableInfo.TOILET_MALE_NAME,
                DBTableInfo.TOILET_FEMALE_NAME,
                DBTableInfo.TOILET_UNISEX_NAME,
                DBTableInfo.TOILET_FACILITY_TYPE_NAME,
                DBTableInfo.TOILET_ACCESS_NOTE_NAME,
                DBTableInfo.TOILET_IS_OPEN_NAME,
                DBTableInfo.TOILET_OPENNING_HOURS_SCHEDULE_NAME,
                DBTableInfo.TOILET_OPENNING_HOURS_NOTE_NAME,
                DBTableInfo.TOILET_BABY_CHANGE_NAME,
                DBTableInfo.TOILET_SHOWERS_NAME,
                DBTableInfo.TOILET_DRINKING_WATER_NAME,
                DBTableInfo.TOILET_SHARPS_DISPOSAL_NAME,
                DBTableInfo.TOILET_SANITARY_DISPOSAL_NAME,
                DBTableInfo.TOILET_LATITUDE_NAME,
                DBTableInfo.TOILET_LONGITUDE_NAME};

        return findPlacesAroundARoute(radius, unit, coordinateList, tableName, fieldNamesInOrder);
    }

    @Override
    public JsonArray findToiletsAroundMidPoint(double latitude, double longitude, int radius, Unit unit) {
        String tableName = DBTableInfo.TOILET_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.TOILET_ID_NAME,
                DBTableInfo.TOILET_NAME_NAME,
                DBTableInfo.TOILET_ADDRESSNOTE_NAME,
                DBTableInfo.TOILET_MALE_NAME,
                DBTableInfo.TOILET_FEMALE_NAME,
                DBTableInfo.TOILET_UNISEX_NAME,
                DBTableInfo.TOILET_FACILITY_TYPE_NAME,
                DBTableInfo.TOILET_ACCESS_NOTE_NAME,
                DBTableInfo.TOILET_IS_OPEN_NAME,
                DBTableInfo.TOILET_OPENNING_HOURS_SCHEDULE_NAME,
                DBTableInfo.TOILET_OPENNING_HOURS_NOTE_NAME,
                DBTableInfo.TOILET_BABY_CHANGE_NAME,
                DBTableInfo.TOILET_SHOWERS_NAME,
                DBTableInfo.TOILET_DRINKING_WATER_NAME,
                DBTableInfo.TOILET_SHARPS_DISPOSAL_NAME,
                DBTableInfo.TOILET_SANITARY_DISPOSAL_NAME,
                DBTableInfo.TOILET_LATITUDE_NAME,
                DBTableInfo.TOILET_LONGITUDE_NAME};

        return findCoordinatesAroundPoint(latitude, longitude, radius, unit, tableName,
                fieldNamesInOrder);
    }



    /*
    Returns the accident nodes around a route.
*/
    public JsonArray findAccidentNodesAroundRoute(int radius, Unit unit, List<Map<String, Double>> coordinateList) {
        String tableName = DBTableInfo.ACCIDENT_NODE_TABLE_NAME;
        String[] fieldNamesInOrder = {DBTableInfo.ACCIDENT_NODE_ID_NAME, DBTableInfo.ACCIDENT_NODE_LATITUDE_NAME,
                DBTableInfo.ACCIDENT_NODE_LONGITUDE_NAME, DBTableInfo.ACCIDENT_NODE_POSTCODE_NAME};

        return findPlacesAroundARoute(radius, unit, coordinateList, tableName, fieldNamesInOrder);
    }


    /*
    Gives a list of places around a route, given a coordinate list.
    What kind of place depends on the table name given.
     */
    private JsonArray findPlacesAroundARoute(int radius, Unit unit,
                                             List<Map<String, Double>> coordinateList,
                                             String tableName, String[] fieldNamesInOrder) {
        JsonArrayBuilder response = Json.createArrayBuilder();
        double latitude;
        double longitude;
        HashSet<String> prevResults = new HashSet<>();

        try (Connection conn = sqlManager.getConnection()) {

            for (Map<String, Double> coord : coordinateList) {
                latitude = coord.get(URLParams.LATITUDE).doubleValue();
                longitude = coord.get(URLParams.LONGITUDE).doubleValue();
                PreparedStatement query = buildQueryToFindCoordinatesWithinRadius("*",
                        tableName, latitude, longitude, unit, radius, conn);

                buildUniqueJsonAccidentNodes(query, response, prevResults, fieldNamesInOrder);
            }
            response.add(StatusCode.success(prevResults.size()));
            if (DEBUG)
                response.add(numResultsInJson(prevResults.size()));

        } catch (SQLException sqlE) {
            return Json.createArrayBuilder().add(StatusCode.error(StatusCode.SQL_ERROR, sqlE.getMessage())).build();
        } catch (Exception e) {
            return Json.createArrayBuilder().add(StatusCode.error(StatusCode.UNKNOWN_ERROR, e.getMessage())).build();
        }

        return response.build();
    }


    /*
    Builds a JsonArray of accident nodes, which are unique.
     */
    private void buildUniqueJsonAccidentNodes(PreparedStatement query, JsonArrayBuilder response, HashSet<String> prevResultIds, String[] fieldNamesInOrder) throws SQLException {
        ResultSet rs = query.executeQuery();
        JsonObjectBuilder json = Json.createObjectBuilder();

        while (rs.next()) {
            String nodeId = rs.getString(DBTableInfo.ACCIDENT_NODE_ID_POS);
            if (!prevResultIds.contains(nodeId)) {
                prevResultIds.add(nodeId);
                for (int i = 1; i <= fieldNamesInOrder.length; i++) {
                    json.add(fieldNamesInOrder[i - 1], rs.getString(i));
                }
                response.add(json.build());
            }
        }
    }


    /* Returns a JsonArray of coordinates around a given point.
    What these coordinates are depends on the table name given.
     */
    private JsonArray findCoordinatesAroundPoint(double latitude, double longitude, int radius, Unit unit, final String tableName, final String[] fieldNamesInOrder) {
        ResultSet rs;
        JsonArrayBuilder response = Json.createArrayBuilder();
        int numResults = 0;

        try (Connection conn = sqlManager.getConnection()) {
            PreparedStatement query = buildQueryToFindCoordinatesWithinRadius("*",
                    tableName, latitude,longitude, unit, radius, conn);
            rs = query.executeQuery();
            while (rs.next()) {
                numResults++;
                JsonObjectBuilder rowJson = Json.createObjectBuilder();
                for (int i = 1; i <= fieldNamesInOrder.length; i++) {
                    rowJson.add(fieldNamesInOrder[i - 1], rs.getString(i));
                }
                response.add(rowJson);
            }
            response.add(StatusCode.success(numResults));
            if (DEBUG)
                response.add(numResultsInJson(numResults));
        } catch (SQLException sqlE) {
            return Json.createArrayBuilder().add(StatusCode.error(StatusCode.SQL_ERROR, sqlE.getMessage())).build();
        }

        return response.build();
    }


    /*
    Assumes that the table has column names "latitude" and "longitude"!
    Algorithm was taken from:
     http://stackoverflow.com/questions/7783684/select-coordinates-which-fall-within-a-radius-of-a-central-point
    Which is based on the math in:
     http://www.movable-type.co.uk/scripts/latlong.html
*/
    private static PreparedStatement buildQueryToFindCoordinatesWithinRadius(String rows, String tableName,
                                                                             double yourLatitude,
                                                                             double yourLongitude,
                                                                             Unit unit,
                                                                             int radius,
                                                                             Connection conn)
            throws SQLException {
        //A different number is used in the calculation depending on whether the units is in km or m.
        int num = unit == Unit.KM ? 6371 : 6371000;

        PreparedStatement query = conn.prepareStatement(String.format("SELECT DISTINCT " +
                "%s FROM %s a WHERE (" +
                " acos(sin(a.latitude * 0.0175) * sin(? * 0.0175) " +
                " + cos(a.latitude * 0.0175) * cos(? * 0.0175) *" +
                " cos((? * 0.0175) - (a.longitude * 0.0175))" +
                ") * %s <= ?);", rows, tableName, num));

        query.setDouble(1, yourLatitude);
        query.setDouble(2, yourLatitude);
        query.setDouble(3, yourLongitude);
        query.setInt(4, radius);

        return query;
    }


    /*
    Returns the number of results given, in JsonObject form.
     */
    private static JsonObject numResultsInJson(int numResults) {
        JsonObjectBuilder json = Json.createObjectBuilder();
        return json.add(JSONParams.NUM_RESULTS, numResults).build();
    }
}
