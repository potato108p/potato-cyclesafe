package au.edu.monash.infotech.cycle.backend.servlets;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletRequest;

import au.edu.monash.infotech.cycle.backend.db.DatabaseQueries;
import au.edu.monash.infotech.cycle.shared.Hour;
import au.edu.monash.infotech.cycle.shared.JSONParams;
import au.edu.monash.infotech.cycle.shared.StatusCode;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.Unit;

public class FilteredAccidentRouteServlet extends RouteServlet {
    @Override
    public JsonArray serveRequest(HashMap<String, String> params, List<Map<String, Double>> coordList) {
        DatabaseQueries dbQueries = DatabaseQueries.getDatabaseQueriesInstance();

        return dbQueries.findAccidentNodesAroundRoute(
                Integer.valueOf(params.get(JSONParams.RADIUS)),
                Unit.valueOf(params.get(JSONParams.UNIT)), coordList,
                Hour.valueOf(params.get(JSONParams.HOUR)));
    }


    @Override
    protected boolean validJson(List<Map<String, Double>> coordList, HttpServletRequest req, HashMap<String, String> params) {
        try {
            JsonReader jsonReader = Json.createReader(req.getInputStream());
            JsonObject jsonObj = jsonReader.readObject();
            params.put(URLParams.RADIUS, jsonObj.getString(JSONParams.RADIUS));
            params.put(URLParams.UNIT, jsonObj.getString(JSONParams.UNIT));
            params.put(JSONParams.HOUR, jsonObj.getString(JSONParams.HOUR));

            for (JsonValue val : jsonObj.getJsonArray(JSONParams.COORDINATE_LIST)) {
                JsonObject jsonCoord = (JsonObject) val;
                Map<String, Double> coord = new HashMap<>();
                coord.put(JSONParams.LATITUDE, Double.valueOf(jsonCoord.getString(JSONParams.LATITUDE)));
                coord.put(JSONParams.LONGITUDE, Double.valueOf(jsonCoord.getString(JSONParams.LONGITUDE)));
                coordList.add(coord);
            }
        } catch (Exception e) {
            log.info("Exception thrown: " + e.getMessage());
            params.put("errorMsg", e.getMessage());
            params.put("statusCode", String.valueOf(StatusCode.JSON_ERROR));
            return false;
        }
        return true;
    }


    /**
     * Checks if the parameters sent are valid - no conversion errors, empty values or missing params.
     * Valid parameter values are added to the params HashMap
     * Any errors are logged in the keys: "statusCode" and "errorMsg"
     *
     * @param params Store all valid parameters or the statusCode and errorMsg if any of the params are invalid.
     * @return true if params are valid
     *         false if params are invalid
     */
    @Override
    protected boolean validRequestParams(HashMap<String, String> params) {
        String radius = params.get(URLParams.RADIUS);
        String unit = params.get(URLParams.UNIT);
        String hour = params.get(URLParams.HOUR);

        if (radius == null) {
            params.put("errorMsg", "radius param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (unit == null) {
            params.put("errorMsg", "unit param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (hour == null) {
            params.put("errorMsg", "hour param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        }


        if ((radius.trim().equals("")) || unit.trim().equals("")
                || (hour.trim().equals(""))) {
            params.put("errorMsg", "Empty radius, latitude, longitude, month, day, hour or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        try {
            Integer.valueOf(radius);
            Unit.valueOf(unit);
            Hour.valueOf(hour);
        } catch (IllegalArgumentException e) {
            log.info("Exception thrown: " + e.getMessage());
            params.put("errorMsg", "Invalid radius, latitude, longitude, hour, month, day or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            params.put("unit", unit);
            return false;
        }

        params.put(URLParams.RADIUS, radius);
        params.put(URLParams.UNIT, unit);
        params.put(URLParams.HOUR, hour);
        return true;
    }
}
