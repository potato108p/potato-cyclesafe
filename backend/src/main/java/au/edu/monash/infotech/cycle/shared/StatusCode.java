package au.edu.monash.infotech.cycle.shared;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 * Created by Moody on 4/04/2017.
 *
 * Status codes and methods for communicating success/failure.
 */

public class StatusCode {
    private StatusCode() {
    }

    public final static int SUCCESS = 0;
    public final static int UNKNOWN_ERROR = 1;
    public final static int SQL_ERROR = 2;
    public final static int MISSING_PARAM = 3;
    public final static int INVALID_PARAM_VAL = 4;
    public final static int JSON_ERROR = 5;
    public final static int SUCCESS_NO_RESULT = 6;

    private final static String DESC_FIELD_NAME = "status-desc";
    private final static String CODE_FIELD_NAME = "status-code";

    /**
     * For sending the status of a successful response to the CycleSafe mobile app request,
     * in JSON format.
     * @return JSON formatted successful status.
     */
    public static JsonObject success(int numResults) {
        JsonObjectBuilder success = Json.createObjectBuilder();
        if (numResults > 0) {
            return success.add(StatusCode.CODE_FIELD_NAME, StatusCode.SUCCESS)
                    .add(StatusCode.DESC_FIELD_NAME, "Success")
                    .build();
        } else {
            return success.add(StatusCode.CODE_FIELD_NAME, StatusCode.SUCCESS_NO_RESULT)
                    .add(StatusCode.DESC_FIELD_NAME, "Successful query, but no results returned.")
                    .build();
        }
    }

    /**
     * For sending the status of a failed response to the CycleSafe mobile app request,
     * in JSON format.
     * @param errorCode One of the codes in the StatusCode constants.
     * @param errorDesc Description of error received.
     * @return JSON formatted failed status.
     */
    public static JsonObject error(int errorCode, String errorDesc) {
        JsonObjectBuilder error = Json.createObjectBuilder();

        return error.add(StatusCode.CODE_FIELD_NAME, errorCode)
                .add(StatusCode.DESC_FIELD_NAME, errorDesc)
                .build();
    }
}
