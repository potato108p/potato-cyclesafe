package au.edu.monash.infotech.cycle.shared;

/**
 * Created by Moody on 6/04/2017.
 */

/**
 * Parameters for common JSON field names - note, this does not apply
 * for query results, whose field names are the same of the column names in the
 * database.
 */
public class JSONParams {
    private JSONParams() {
    }

    public final static String LATITUDE = URLParams.LATITUDE;
    public final static String LONGITUDE = URLParams.LONGITUDE;
    public final static String RADIUS = "r";
    public final static String UNIT = "u";
    public final static String COORDINATE_LIST = "coords";
    public final static String HOUR = URLParams.HOUR;
    public final static String DAY = URLParams.DAY;
    public final static String MONTH = URLParams.MONTH;

    /* FOR DEBUGGING ONLY */
    public final static String NUM_RESULTS ="num-results";
}
