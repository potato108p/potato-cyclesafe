package au.edu.monash.infotech.cycle.backend.servlets;

import java.util.HashMap;

import javax.json.JsonArray;
import javax.servlet.http.HttpServletRequest;

import au.edu.monash.infotech.cycle.backend.db.DatabaseQueries;
import au.edu.monash.infotech.cycle.shared.Hour;
import au.edu.monash.infotech.cycle.shared.StatusCode;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.Unit;


public class FilteredAccidentServlet extends SinglePointServlet {
    @Override
    public JsonArray serveRequest(HashMap<String, String> params) {
        DatabaseQueries dbQueries = DatabaseQueries.getDatabaseQueriesInstance();

        return dbQueries.findAccidentNodesAroundMidPoint(
                Double.valueOf(params.get(URLParams.LATITUDE)),
                Double.valueOf(params.get(URLParams.LONGITUDE)),
                Integer.valueOf(params.get(URLParams.RADIUS)),
                Unit.valueOf(params.get(URLParams.UNIT)),
                Hour.valueOf(params.get(URLParams.HOUR)));
    }

    @Override
    protected boolean validRequestParams(HashMap<String, String> params, HttpServletRequest req) {
        String latitude = req.getParameter(URLParams.LATITUDE);
        String longitude = req.getParameter(URLParams.LONGITUDE);
        String radius = req.getParameter(URLParams.RADIUS);
        String unit = req.getParameter(URLParams.UNIT);
        String hour = req.getParameter(URLParams.HOUR);

        if (latitude == null) {
            params.put("errorMsg", "latitude param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (longitude == null) {
            params.put("errorMsg", "longitude param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (radius == null) {
            params.put("errorMsg", "radius param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (unit == null) {
            params.put("errorMsg", "unit param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (hour == null) {
            params.put("errorMsg", "hour param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        }


        if ((latitude.trim().equals("")) || (longitude.trim().equals("")) ||
                (radius.trim().equals("")) || unit.trim().equals("") || (hour.trim().equals(""))) {
            params.put("errorMsg", "Empty radius, latitude, longitude, month, day, hour or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        try {
            Integer.valueOf(radius);
            Double.valueOf(latitude);
            Double.valueOf(longitude);
            Unit.valueOf(unit);
            Hour.valueOf(hour);
        } catch (IllegalArgumentException e) {
            log.info("Exception thrown: " + e.getMessage());
            params.put("errorMsg", "Invalid radius, latitude, longitude, hour or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            params.put("unit", unit);
            return false;
        }

        params.put(URLParams.LATITUDE, latitude);
        params.put(URLParams.LONGITUDE, longitude);
        params.put(URLParams.RADIUS, radius);
        params.put(URLParams.UNIT, unit);
        params.put(URLParams.HOUR, hour);

        return true;
    }
}
