package au.edu.monash.infotech.cycle.backend.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.edu.monash.infotech.cycle.backend.db.DatabaseQueries;
import au.edu.monash.infotech.cycle.shared.JSONParams;
import au.edu.monash.infotech.cycle.shared.StatusCode;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Validates the client POST request and sends the response as a JSONArray.
 * In this servlet, the POST request body from the client is a JSON object.
 */
abstract class RouteServlet extends HttpServlet {
    private DatabaseQueries dbQueries;
    protected static final Logger log = Logger.getLogger(RouteServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/plain");
        resp.getWriter().println("You're not supposed to be here.");
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        dbQueries = DatabaseQueries.getDatabaseQueriesInstance();
        List<Map<String, Double>> coordList = new LinkedList<>();
        HashMap<String, String> params = new HashMap<>();

        if ((validJson(coordList, req, params)) &&  (validRequestParams(params))) {
            resp.setContentType("application/json");
            resp.getWriter().println(serveRequest(params, coordList));
        } else {
            resp.setContentType("application/json");
            JsonArrayBuilder errorResp = Json.createArrayBuilder();
            errorResp.add(StatusCode.error(Integer.valueOf(params.get("statusCode")),
                    params.get("errorMsg")));
            resp.getWriter().println(errorResp.build());
        }
        resp.getWriter().close();
    }


    public abstract JsonArray serveRequest(HashMap<String, String> params, List<Map<String, Double>> coordList);


    /**
     * Checks if the JSON values sent are valid.
     *
     * @param coordList To store the coordinates read from the JSON, if they are valid. The coordinates are for
     *                  the route.
     * @param req The request, which contains the client sent JSON values
     * @param params The search radius distance and unit of the distance.
     * @return
     */
    protected boolean validJson(List<Map<String, Double>> coordList, HttpServletRequest req, HashMap<String, String> params) {
        try {
            JsonReader jsonReader = Json.createReader(req.getInputStream());
            JsonObject jsonObj = jsonReader.readObject();
            params.put(URLParams.RADIUS, jsonObj.getString(JSONParams.RADIUS));
            params.put(URLParams.UNIT, jsonObj.getString(JSONParams.UNIT));

            for (JsonValue val : jsonObj.getJsonArray(JSONParams.COORDINATE_LIST)) {
                JsonObject jsonCoord = (JsonObject) val;
                Map<String, Double> coord = new HashMap<>();
                coord.put(JSONParams.LATITUDE, Double.valueOf(jsonCoord.getString(JSONParams.LATITUDE)));
                coord.put(JSONParams.LONGITUDE, Double.valueOf(jsonCoord.getString(JSONParams.LONGITUDE)));
                coordList.add(coord);
            }
        } catch (Exception e) {
            log.info("Exception thrown: " + e.getMessage());
            params.put("errorMsg", e.getMessage());
            params.put("statusCode", String.valueOf(StatusCode.JSON_ERROR));
            return false;
        }
        return true;
    }


    /**
     * Checks if the parameters sent are valid - no conversion errors, empty values or missing params.
     * Valid parameter values are added to the params HashMap
     * Any errors are logged in the keys: "statusCode" and "errorMsg"
     *
     * @param params Store all valid parameters or the statusCode and errorMsg if any of the params are invalid.
     * @return true if params are valid
     *         false if params are invalid
     */
    protected boolean validRequestParams(HashMap<String, String> params) {
        String radius = params.get(URLParams.RADIUS);
        String unit = params.get(URLParams.UNIT);

        if (radius == null) {
            params.put("errorMsg", "radius param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (unit == null) {
            params.put("errorMsg", "unit param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        }

        if ((radius.trim().equals("")) || unit.trim().equals("")) {
            params.put("errorMsg", "Empty radius or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }
        try {
            Integer.valueOf(radius);
            Unit.valueOf(unit);
        } catch (IllegalArgumentException e) {
            log.info("Exception thrown: " + e.getMessage());
            params.put("errorMsg", "Invalid radius or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        if (Integer.valueOf(radius) <= 0) {
            params.put("errorMsg", "Radius cannot be negative or zero");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        params.put(URLParams.RADIUS, radius);
        params.put(URLParams.UNIT, unit);
        return true;
    }
}
