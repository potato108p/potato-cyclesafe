package au.edu.monash.infotech.cycle.shared;

/**
 * Created by Moody on 18/04/2017.
 *
 * Enum for the days of the week. Each day is represented by a number.
 */

public enum DayOfWeek {
    SUNDAY(1),
    MONDAY(2),
    TUESDAY(3),
    WEDNESDAY(4),
    THURSDAY(5),
    FRIDAY(6),
    SATURDAY(7);

    private int dayNumber;
    private static DayOfWeek[] days = {null, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY};

    DayOfWeek(int val) {this.dayNumber = val;}

    @Override
    public String toString() {
        return String.valueOf(dayNumber);
    }

    public static DayOfWeek getDayOfWeek(int index){
        return days[index];
    }

    public int getDayNumber() {
        return dayNumber;
    }
}
