package au.edu.monash.infotech.cycle.backend.servlets;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.JsonArray;

import au.edu.monash.infotech.cycle.backend.db.DatabaseQueries;
import au.edu.monash.infotech.cycle.shared.JSONParams;
import au.edu.monash.infotech.cycle.shared.Unit;

public class BuildingWithBicycleParkingtRouteServlet extends RouteServlet {
    @Override
    public JsonArray serveRequest(HashMap<String, String> params, List<Map<String, Double>> coordList) {
        DatabaseQueries dbQueries = DatabaseQueries.getDatabaseQueriesInstance();

        return dbQueries.findBuildingsWithBicycleParkingAroundRoute(
                Integer.valueOf(params.get(JSONParams.RADIUS)),
                Unit.valueOf(params.get(JSONParams.UNIT)), coordList);
    }
}
