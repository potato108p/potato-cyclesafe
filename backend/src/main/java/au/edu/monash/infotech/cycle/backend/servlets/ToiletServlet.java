package au.edu.monash.infotech.cycle.backend.servlets;

import java.util.HashMap;
import javax.json.JsonArray;
import au.edu.monash.infotech.cycle.backend.db.DatabaseQueries;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.Unit;

public class ToiletServlet extends SinglePointServlet {
    @Override
    public JsonArray serveRequest(HashMap<String, String> params) {
        DatabaseQueries dbQueries = DatabaseQueries.getDatabaseQueriesInstance();
        return dbQueries.findToiletsAroundMidPoint(
                Double.valueOf(params.get(URLParams.LATITUDE)),
                Double.valueOf(params.get(URLParams.LONGITUDE)),
                Integer.valueOf(params.get(URLParams.RADIUS)),
                Unit.valueOf(params.get(URLParams.UNIT)));
    }
}
