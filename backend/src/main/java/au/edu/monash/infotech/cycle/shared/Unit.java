package au.edu.monash.infotech.cycle.shared;

/**
 * Created by Moody on 4/04/2017.
 * Enum for representing distance measurement units.
 */

public enum Unit {
    KM("km"),
    M("m");

    private String name;

    Unit(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
