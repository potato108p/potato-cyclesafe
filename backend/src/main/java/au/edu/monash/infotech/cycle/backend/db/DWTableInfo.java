package au.edu.monash.infotech.cycle.backend.db;

/**
 * Created by Moody on 18/04/2017.
 * List of all the tables in the database, specifically the data warehouse,
 * including the field names and positions.
 */

class DWTableInfo {
    private DWTableInfo() {
    }

    protected final static String ACCIDENT_FACT_TABLE_NAME = "accident_fact";
    protected final static String ACCIDENT_FACT_ACCIDENT_TYPE_ID_NAME = "accident_type_id";
    protected final static int ACCIDENT_FACT_ACCIDENT_TYPE_ID_POS = 1;
    protected final static String ACCIDENT_FACT_MONTH_ID_NAME = "month_id";
    protected final static int ACCIDENT_FACT_MONTH_ID_POS = 2;
    protected final static String ACCIDENT_FACT_NODE_ID_NAME = "node_id";
    protected final static int ACCIDENT_FACT_NODE_ID_POS = 3;
    protected final static String ACCIDENT_FACT_TIMEPERIOD_ID_NAME = "time_period_id";
    protected final static int ACCIDENT_FACT_TIMEPERIOD_ID_POS = 4;
    protected final static String ACCIDENT_FACT_DAY_ID_NAME = "day_id";
    protected final static int ACCIDENT_FACT_DAY_ID_POS = 5;
    protected final static String ACCIDENT_FACT_NUM_ACCIDENTS_NAME = "num_accidents";
    protected final static int ACCIDENT_FACT_NUM_ACCIDENTS_POS = 6;
    protected final static String ACCIDENT_FACT_NUM_INJURED_NAME = "num_injured";
    protected final static int ACCIDENT_FACT_NUM_INJURED_POS = 7;

    protected final static String DAY_DIM_TABLE_NAME = "day_dim";
    protected final static String DAY_DIM_DAY_ID_NAME = "day_id";
    protected final static int DAY_DIM_DAY_ID_POS = 1;
    protected final static String DAY_DIM_DAY_DESC_NAME = "day_desc";
    protected final static int DAY_DIM_DAY_DESC_POS = 2;

    protected final static String ACCIDENT_TYPE_DIM_TABLE_NAME = "accident_type_dim";
    protected final static String ACCIDENT_TYPE_DIM_ACCIDENT_TYPE_ID_NAME = "accident_type_id";
    protected final static int ACCIDENT_TYPE_DIM_ACCIDENT_TYPE_ID_POS = 1;
    protected final static String ACCIDENT_TYPE_DIM_ACCIDENT_TYPE_DESC_NAME = "accident_type_desc";
    protected final static int ACCIDENT_TYPE_DIM_ACCIDENT_TYPE_DESC_POS = 2;

    protected final static String MONTH_DIM_TABLE_NAME = "month_dim";
    protected final static String MONTH_DIM_MONTH_ID_NAME = "month_id";
    protected final static int MONTH_DIM_MONTH_ID_POS = 1;
    protected final static String MONTH_DIM_MONTH_DESC_NAME = "month_desc";
    protected final static int MONTH_DIM_MONTH_DESC_POS = 2;

    protected final static String TIME_PERIOD_DIM_TABLE_NAME = "time_period_dim";
    protected final static String TIME_PERIOD_DIM_TIME_PERIOD_ID_NAME = "time_period_id";
    protected final static int TIME_PERIOD_DIM_TIME_PERIOD_ID_POS = 1;
    protected final static String TIME_PERIOD_DIM_START_TIME_INCUSIVE_NAME = "start_time_inclusive";
    protected final static int TIME_PERIOD_DIM_START_TIME_INCUSIVE_POS = 2;
    protected final static String TIME_PERIOD_DIM_END_TIME_EXCUSIVE_NAME = "end_time_exclusive";
    protected final static int TIME_PERIOD_DIM_END_TIME_EXCUSIVE_POS = 3;
}
