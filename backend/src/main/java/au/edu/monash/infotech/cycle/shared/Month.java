package au.edu.monash.infotech.cycle.shared;

/**
 * Created by Moody on 18/04/2017.
 *
 * Enum for each month of the year.
 */

public enum Month {
    JANUARY(1),
    FEBRUARY(2),
    MARCH(3),
    APRIL(4),
    MAY(5),
    JUNE(6),
    JULY(7),
    AUGUST(8),
    SEPTEMBER(9),
    OCTOBER(10),
    NOVEMBER(11),
    DECEMBER(12);

    private int monthNum;
    private static Month[] months = {null, JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY,
            AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER};

    Month(int val) {this.monthNum = val;}

    @Override
    public String toString() {
        return String.valueOf(monthNum);
    }

    public static Month getMonth(int index){
        return months[index];
    }

    public int getMonthNum() {
        return monthNum;
    }
}
