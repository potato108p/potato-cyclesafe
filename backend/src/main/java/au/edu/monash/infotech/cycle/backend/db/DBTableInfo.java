package au.edu.monash.infotech.cycle.backend.db;

/**
 * List of all the tables in the database, including the field names and positions.
 */

class DBTableInfo {

    private DBTableInfo() {
    }

    protected static final String ACCIDENT_NODE_TABLE_NAME = "node_accidents";
    protected static final String ACCIDENT_NODE_ID_NAME = "nodeid";
    protected static final int ACCIDENT_NODE_ID_POS = 1;
    protected static final String ACCIDENT_NODE_LATITUDE_NAME = "latitude";
    protected static final int ACCIDENT_NODE_LATITUDE_POS = 2;
    protected static final String ACCIDENT_NODE_LONGITUDE_NAME = "longitude";
    protected static final int ACCIDENT_NODE_LONGITUDE_POS = 3;
    protected static final String ACCIDENT_NODE_POSTCODE_NAME = "postcode";
    protected static final int ACCIDENT_NODE_POSTCODE_POS = 4;

    protected static final String TOILET_TABLE_NAME = "toilet_vic";
    protected static final String TOILET_ID_NAME = "id";
    protected static final int TOILET_ID_POS = 1;
    protected static final String TOILET_NAME_NAME = "name";
    protected static final int TOILET_NAME_POS = 2;
    protected static final String TOILET_ADDRESSNOTE_NAME = "address_note";
    protected static final int TOILET_ADDRESSNOTE_POS = 3;
    protected static final String TOILET_MALE_NAME = "male";
    protected static final int TOILET_MALE_POS = 4;
    protected static final String TOILET_FEMALE_NAME = "female";
    protected static final int TOILET_FEMALE_POS = 5;
    protected static final String TOILET_UNISEX_NAME = "unisex";
    protected static final int TOILET_UNISEX_POS = 6;
    protected static final String TOILET_FACILITY_TYPE_NAME = "facility_type";
    protected static final int TOILET_FACILITY_TYPE_POS = 7;
    protected static final String TOILET_ACCESS_NOTE_NAME = "access_note";
    protected static final int TOILET_ACCESS_NOTE_POS = 8;
    protected static final String TOILET_IS_OPEN_NAME = "is_open";
    protected static final int TOILET_IS_OPEN_POS = 9;
    protected static final String TOILET_OPENNING_HOURS_SCHEDULE_NAME = "openning_hours_schedule";
    protected static final int TOILET_OPENNING_HOURS_SCHEDULE_POS = 10;
    protected static final String TOILET_OPENNING_HOURS_NOTE_NAME = "openning_hours_note";
    protected static final int TOILET_OPENNING_HOURS_NOTE_POS = 11;
    protected static final String TOILET_BABY_CHANGE_NAME = "baby_change";
    protected static final int TOILET_BABY_CHANGE_POS = 12;
    protected static final String TOILET_SHOWERS_NAME = "showers";
    protected static final int TOILET_SHOWERS_POS = 13;
    protected static final String TOILET_DRINKING_WATER_NAME = "drinking_water";
    protected static final int TOILET_DRINKING_WATER_POS = 14;
    protected static final String TOILET_SHARPS_DISPOSAL_NAME = "sharps_disposal";
    protected static final int TOILET_SHARPS_DISPOSAL_POS = 15;
    protected static final String TOILET_SANITARY_DISPOSAL_NAME = "sanitary_disposal";
    protected static final int TOILET_SANITARY_DISPOSAL_POS = 16;
    protected static final String TOILET_LATITUDE_NAME = "latitude";
    protected static final int TOILET_LATITUDE_POS = 17;
    protected static final String TOILET_LONGITUDE_NAME = "longitude";
    protected static final int TOILET_LONGITUDE_POS = 18;

    protected static final String FOUNTAINS_TABLE_NAME = "fountains";
    protected static final String FOUNTAINS_LATITUDE_NAME = "latitude";
    protected static final int FOUNTAINS_LATITUDE_POS = 1;
    protected static final String FOUNTAINS_LONGITUDE_NAME = "longitude";
    protected static final int FOUNTAINS_LONGITUDE_POS = 2;
    protected static final String FOUNTAINS_ID_NAME = "id";
    protected static final int FOUNTAINS_ID_POS = 3;

    protected final static String CYCLERAILS_TABLE_NAME = "cycle_rails";
    protected final static String CYCLERAILS_DESC_NAME = "description";
    protected final static int CYCLERAILS_DESC_POS = 1;
    protected final static String CYCLERAILS_LONGITUDE_NAME = "longitude";
    protected final static int CYCLERAILS_LONGITUDE_POS = 2;
    protected final static String CYCLERAILS_LATITUDE_NAME = "latitude";
    protected final static int CYCLERAILS_LATITUDE_POS = 3;
    protected final static String CYCLERAILS_ID_NAME = "id";
    protected final static int CYCLERAILS_ID_POS = 4;

    protected final static String BUILDINGS_WITH_BICYCLE_FAC_TABLE_NAME = "building_bicycles";
    protected final static String BUILDINGS_WITH_BICYCLE_FAC_PROPERTY_ID_NAME = "property_id";
    protected final static int BUILDINGS_WITH_BICYCLE_FAC_PROPERTY_ID_POS = 1;
    protected final static String BUILDINGS_WITH_BICYCLE_FAC_DESCRIPTION_NAME = "description";
    protected final static int BUILDINGS_WITH_BICYCLE_FAC_DESCRIPTION_POS = 2;
    protected final static String BUILDINGS_WITH_BICYCLE_FAC_NUM_SPACES_NAME = "num_spaces";
    protected final static int BUILDINGS_WITH_BICYCLE_FAC_NUM_SPACES_POS  = 3;
    protected final static String BUILDINGS_WITH_BICYCLE_FAC_LATITUDE_NAME = "latitude";
    protected final static int BUILDINGS_WITH_BICYCLE_FAC_LATITUDE_POS  = 4;
    protected final static String BUILDINGS_WITH_BICYCLE_FAC_LONGITUDE_NAME = "longitude";
    protected final static int BUILDINGS_WITH_BICYCLE_FAC_LONGITUDE_POS  = 5;

    protected final static String SUBURB_THEFT_TABLE_NAME = "suburb_theft";
    protected final static String SUBURB_THEFT_POSTCODE_NAME = "postcode";
    protected final static int SUBURB_THEFT_POSTCODE_POS = 1;
    protected final static String SUBURB_THEFT_NUM_OFFENSES_NAME = "num_offenses";
    protected final static int SUBURB_THEFT_NUM_OFFENSES_POS = 2;
    protected final static String SUBURB_THEFT_SAFETY_DESC_NAME = "safety_desc";
    protected final static int SUBURB_THEFT_SAFETY_DESC_POS  = 3;

    protected final static String SCHOOL_TABLE_NAME = "school_vic";
    protected final static String SCHOOL_ID_NAME = "id";
    protected final static int SCHOOL_ID_POS = 1;
    protected final static String SCHOOL_NAME_NAME = "name";
    protected final static int SCHOOL_NAME_POS = 2;
    protected final static String SCHOOL_LONGITUDE_NAME = "longitude";
    protected final static int SCHOOL_LONGITUDE_POS = 3;
    protected final static String SCHOOL_LATITUDE_NAME = "latitude";
    protected final static int SCHOOL_LATITUDE_POS = 4;
}
