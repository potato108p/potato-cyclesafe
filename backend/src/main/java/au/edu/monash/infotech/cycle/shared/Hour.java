package au.edu.monash.infotech.cycle.shared;

/**
 * Created by Moody on 18/04/2017.
 *
 * Enum for every hour of the day. The names are in 12 hour format, while the integers used
 * to represent them are in 24 hour format.
 */

public enum Hour {
    TWELVE_AM(0),
    ONE_AM(1),
    TWO_AM(2),
    THREE_AM(3),
    FOUR_AM(4),
    FIVE_AM(5),
    SIX_AM(6),
    SEVEN_AM(7),
    EIGHT_AM(8),
    NINE_AM(9),
    TEN_AM(10),
    ELEVEN_AM(11),
    TWELVE_PM(12),
    ONE_PM(13),
    TWO_PM(14),
    THREE_PM(15),
    FOUR_PM(16),
    FIVE_PM(17),
    SIX_PM(18),
    SEVEN_PM(19),
    EIGHT_PM(20),
    NINE_PM(21),
    TEN_PM(22),
    ELEVEN_PM(23);

    private int val;
    private static Hour[] time = {TWELVE_AM, ONE_AM, TWO_AM, THREE_AM, FOUR_AM, FIVE_AM, SIX_AM, SEVEN_AM, EIGHT_AM, NINE_AM, TEN_AM, ELEVEN_AM,
            TWELVE_PM, ONE_PM, TWO_PM, THREE_PM, FOUR_PM, FIVE_PM, SIX_PM, SEVEN_PM, EIGHT_PM, NINE_PM, TEN_PM, ELEVEN_PM};

    Hour(int val) {this.val = val;}

    @Override
    public String toString() {
        return String.valueOf(val);
    }

    public static Hour getHour(int index){
        return time[index];
    }

    public int getVal() {
        return val;
    }
}
