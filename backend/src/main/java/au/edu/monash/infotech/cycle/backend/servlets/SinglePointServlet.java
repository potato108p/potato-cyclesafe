package au.edu.monash.infotech.cycle.backend.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.edu.monash.infotech.cycle.shared.StatusCode;
import au.edu.monash.infotech.cycle.shared.URLParams;
import au.edu.monash.infotech.cycle.shared.Unit;

/**
 * Created by Moody on 9/04/2017.
 *
 * Validates the client POST request and sends the response as a JSONArray.
 * In this servlet, the POST request values from the client is sent as URL parameters.
 */

abstract class SinglePointServlet extends HttpServlet {
    protected static final Logger log = Logger.getLogger(RouteServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/plain");
        resp.getWriter().println("You're not supposed to be here.");
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        HashMap<String, String> params = new HashMap<>();

        if (validRequestParams(params, req)) {
            resp.setContentType("application/json");
            resp.getWriter().println(serveRequest(params));
        } else {
            resp.setContentType("application/json");
            JsonArrayBuilder errorResp = Json.createArrayBuilder();
            errorResp.add(StatusCode.error(Integer.valueOf(params.get("statusCode")),
                    params.get("errorMsg")));
            resp.getWriter().println(errorResp.build());
        }
        resp.getWriter().close();
    }


    /**
     * Checks if the parameters sent are valid - no conversion errors, empty values or missing params.
     * Valid parameter values are added to the params HashMap
     * Any errors are logged in the keys: "statusCode" and "errorMsg"
     *
     * @param params Store all valid parameters or the statusCode and errorMsg if any of the params are invalid.
     * @param req Request holding parameters.
     * @return true if params are valid
     *         false if params are invalid
     */
    protected boolean validRequestParams(HashMap<String, String> params, HttpServletRequest req) {
        String latitude = req.getParameter(URLParams.LATITUDE);
        String longitude = req.getParameter(URLParams.LONGITUDE);
        String radius = req.getParameter(URLParams.RADIUS);
        String unit = req.getParameter(URLParams.UNIT);

        if (latitude == null) {
            params.put("errorMsg", "latitude param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (longitude == null) {
            params.put("errorMsg", "longitude param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (radius == null) {
            params.put("errorMsg", "radius param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        } else if (unit == null) {
            params.put("errorMsg", "unit param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        }

        if ((latitude.trim().equals("")) || (longitude.trim().equals("")) || (radius.trim().equals("")) || unit.trim().equals("")) {
            params.put("errorMsg", "Empty radius, latitude, longitude or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        try {
            Integer.valueOf(radius);
            Double.valueOf(latitude);
            Double.valueOf(longitude);
            Unit.valueOf(unit);
        } catch (IllegalArgumentException e) {
            log.info("Exception thrown: " + e.getMessage());
            params.put("errorMsg", "Invalid radius, latitude, longitude or unit value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        if (Integer.valueOf(radius) <= 0) {
            params.put("errorMsg", "Radius cannot be negative or zero");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        params.put(URLParams.LATITUDE, latitude);
        params.put(URLParams.LONGITUDE, longitude);
        params.put(URLParams.RADIUS, radius);
        params.put(URLParams.UNIT, unit);

        return true;
    }

    public abstract JsonArray serveRequest(HashMap<String, String> params);
}
