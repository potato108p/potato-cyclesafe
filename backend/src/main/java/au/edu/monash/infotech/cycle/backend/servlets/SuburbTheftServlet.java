package au.edu.monash.infotech.cycle.backend.servlets;

import java.util.HashMap;

import javax.json.JsonArray;
import javax.servlet.http.HttpServletRequest;

import au.edu.monash.infotech.cycle.backend.db.DatabaseQueries;
import au.edu.monash.infotech.cycle.shared.StatusCode;
import au.edu.monash.infotech.cycle.shared.URLParams;


public class SuburbTheftServlet extends SinglePointServlet {
    @Override
    public JsonArray serveRequest(HashMap<String, String> params) {
        DatabaseQueries dbQueries = DatabaseQueries.getDatabaseQueriesInstance();
        return dbQueries.getSafetyRatingForSuburb(Integer.valueOf(params.get(URLParams.POSTCODE)));
    }


    @Override
    protected boolean validRequestParams(HashMap<String, String> params, HttpServletRequest req) {
        String postcode = req.getParameter(URLParams.POSTCODE);

        if (postcode == null) {
            params.put("errorMsg", "postcode param is missing.");
            params.put("statusCode", String.valueOf(StatusCode.MISSING_PARAM));
            return false;
        }

        if (postcode.trim().equals("")) {
            params.put("errorMsg", "Empty postcode value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        try {
            Integer p = Integer.valueOf(postcode);

            if (!((p >= 3000) && (p < 4000))) {
                params.put("errorMsg", "Not a valid Victorian postcode");
                params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
                return false;
            }
        } catch (IllegalArgumentException e) {
            log.info("Exception thrown: " + e.getMessage());
            params.put("errorMsg", "Invalid postcode value");
            params.put("statusCode", String.valueOf(StatusCode.INVALID_PARAM_VAL));
            return false;
        }

        params.put(URLParams.POSTCODE, postcode);

        return true;
    }
}
