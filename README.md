# Summary #
Android mobile application and Java Servlets developed for the Monash Postgraduate IT Industry Experience Project, by team Potato.

_(Semester 1, 2017)_

## Project Description ##
> Topic: Safety First
>
> Cycling has grown tremendously in Melbourne over the last decade, and the Victorian government has undertaken initiatives to create new infrastructure for cyclists to accommodate this demand. With this growth, there has been an increase in cycle-related accidents as more cyclists share roads with vehicles and pedestrians. Currently, most cycling-related applications address social networking, fitness and activity planning but none address safety. CycleSafe is an Android app that aims to address this gap for cyclists in Melbourne.
>
> CycleSafe allows users to plan safe cycling journeys by identifying accident hotspots and school zones along their route, as well as providing cycle theft and weather information for their start and destination points. It also provides other information that cyclists will find useful, such as nearby public toilets, drinking fountains and bicycle parking.

## Developers ##
* **James Hongsong Cao**
> * Android app development

* **Jithya Nanayakkara**
> * Java servlets for the backend app _(hosted on Google Cloud AppEngine)_
> * MySQL data warehouse _(hosted on Google Cloud SQL)_
> * Cache on mobile app


## Further Information ##
http://bit.ly/potato-cyclesafe